package ru.konungstvo.kmrp_lore;

import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;

@Mod(modid = Core.MODID, name = Core.NAME, version = Core.VERSION, acceptableRemoteVersions = "*")
public class Core {
    public static final String MODID = "kmrplore";
    public static final String NAME = "KMRP Lore";
    public static final String VERSION = "0.1";
    @SidedProxy(
            clientSide = "ru.konungstvo.kmrp_lore.ClientProxy",
            serverSide = "ru.konungstvo.kmrp_lore.CommonProxy"
    )
    protected static CommonProxy proxy;

    @Mod.EventHandler
    public void init(FMLInitializationEvent event) {
        proxy.init(event);
    }

    @Mod.EventHandler
    public void onServerStart(FMLServerStartingEvent event) {
        proxy.startServer(event);
    }
}
