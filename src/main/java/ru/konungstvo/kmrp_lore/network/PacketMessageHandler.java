package ru.konungstvo.kmrp_lore.network;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.relauncher.Side;
import org.json.simple.parser.ParseException;
import ru.konungstvo.kmrp_lore.helpers.GmTagHandler;

import java.io.IOException;

// The params of the IMessageHandler are <REQ, REPLY>
// This means that the first param is the packet you are receiving, and the second is the packet you are returning.
// The returned packet can be used as a "response" from a sent packet.

public class PacketMessageHandler implements IMessageHandler<PacketMessage, IMessage> {
    // Do note that the default constructor is required, but implicitly defined in this case

    @Override
    public IMessage onMessage(PacketMessage message, MessageContext ctx) {
        //System.out.println("Recieving packet on " + FMLCommonHandler.instance().getSide());
        // This is the player the packet was sent to the server from


        if (FMLCommonHandler.instance().getSide().equals(Side.SERVER)) {
            EntityPlayerMP serverPlayer = ctx.getServerHandler().player;
            String lore = "error";
            try {
                lore = GmTagHandler.getStringFromTag(message.customPacket);
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (lore == null || lore.isEmpty()) lore = "ERROR: Неверный тег или другая ошибка!";
            TextComponentString result = new TextComponentString("Описание тега ");
            result.getStyle().setColor(TextFormatting.GRAY);

            TextComponentString tag = new TextComponentString("[" + message.customPacket + "]:\n");
            tag.getStyle().setColor(TextFormatting.AQUA);
            result.appendSibling(tag);

            TextComponentString loretext = new TextComponentString(lore);
            loretext.getStyle().setColor(TextFormatting.GOLD);
            result.appendSibling(loretext);

            serverPlayer.sendMessage(result);
        }

        return null;

    }
}

