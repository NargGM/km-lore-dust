package ru.konungstvo.kmrp_lore.network;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import ru.konungstvo.kmrp_lore.Core;

public class KMPacketHandler {
        public static final SimpleNetworkWrapper INSTANCE = NetworkRegistry.INSTANCE.newSimpleChannel(Core.MODID);
        private static int DISCRIMINATOR_ID = 0;
        public static int getDiscriminatorId() {
            return DISCRIMINATOR_ID++;
        }

}
