package ru.konungstvo.kmrp_lore;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.init.MobEffects;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.nbt.NBTTagString;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.client.ClientCommandHandler;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.entity.player.ItemTooltipEvent;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.server.permission.DefaultPermissionLevel;
import net.minecraftforge.server.permission.PermissionAPI;
import ru.konungstvo.kmrp_lore.command.EquipmentBuffExecutor;
import ru.konungstvo.kmrp_lore.command.GmTagCommand;
import ru.konungstvo.kmrp_lore.helpers.*;
import ru.konungstvo.kmrp_lore.network.KMPacketHandler;
import ru.konungstvo.kmrp_lore.network.PacketMessage;
import ru.konungstvo.kmrp_lore.network.PacketMessageHandler;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static ru.konungstvo.kmrp_lore.helpers.WeaponDurabilityHandler.DURABILITY_DICT_COMPOUND;
import static ru.konungstvo.kmrp_lore.helpers.WeightHandler.WEIGHT;

public class ClientProxy extends CommonProxy {
    public static NBTTagCompound copiedLore = null;
    public static final List<String> excludedStrings = Arrays.asList(
            "Когда",
            "Урон",
            "Скорость"
    );
    Pattern rollpattern1 = Pattern.compile("(\\d+d\\d+)|(d\\d+)");
    private static HashMap<Integer, String> skillTable = new HashMap<>();

    static private ItemStack lastItemStack = null;

    public ClientProxy() {
        MinecraftForge.EVENT_BUS.register(this);
    }

    public void init(FMLInitializationEvent event) {
        MinecraftForge.EVENT_BUS.register(this);
        System.out.println("Registered packet handler");
        ClientCommandHandler.instance.registerCommand(new GmTagCommand());
        KMPacketHandler.INSTANCE.registerMessage(PacketMessageHandler.class, PacketMessage.class, 1, Side.SERVER);
        PermissionAPI.registerNode("km.gm", DefaultPermissionLevel.OP, "Game Master permission");

        skillTable.put(-5, "абсолютно ублюдски");
        skillTable.put(-4, "ужасно---");
        skillTable.put(-3, "ужасно--");
        skillTable.put(-2, "ужасно-");
        skillTable.put(-1, "ужасно");
        skillTable.put(0, "плохо");
        skillTable.put(1, "посредственно");
        skillTable.put(2, "нормально");
        skillTable.put(3, "хорошо");
        skillTable.put(4, "отлично");
        skillTable.put(5, "превосходно");
        skillTable.put(6, "легендарно");
        skillTable.put(7, "легендарно+");
        skillTable.put(8, "легендарно++");
        skillTable.put(9, "легендарно+++");
        skillTable.put(10, "КАК АЛЛАХ");

    }

    public String getResultAsString(int level) {
        //return DataHolder.getInstance().getSkillTable().get(result) + " [" + DataHolder.getInstance().getSkillTable().get(firstResult) + "]";
        return skillTable.get(level);
    }

    @SubscribeEvent
    public void tooltip_event(ItemTooltipEvent event) {
        List<String> toRemove = new ArrayList<>();
        for (String str : event.getToolTip()) {
            if (str.isEmpty()) {
                toRemove.add(str);
                continue;
            }
            for (String ex : excludedStrings) {
                if (str.contains(ex)) {
                    toRemove.add(str);
                }
            }
        }

        if (WeaponTagsHandler.hasWeaponTags(event.getItemStack())) {
            event.getToolTip().removeAll(toRemove);
            boolean sneak = GuiScreen.isShiftKeyDown();
            WeaponTagsHandler weaponTagsHandler = new WeaponTagsHandler(event.getItemStack());

            NBTTagCompound weaponTags = weaponTagsHandler.getWeaponTags();

            event.getToolTip().add("");
            event.getToolTip().add(TextFormatting.RED + "Теги оружия");
            weaponTagsHandler.setDefaultTagIfNone();
            // CREATE WEAPON OVERVIEW FOR TOOLTIP
            for (String key : weaponTags.getKeySet()) {
                if (key.equals("commonDurability")) continue;
                if (key.equals(WeaponTagsHandler.DEFAULT_WEAPON)) continue;

                TextFormatting color = TextFormatting.DARK_AQUA;
                if (weaponTagsHandler.isWeaponDefault(key)) color = TextFormatting.AQUA;

                NBTTagCompound weapon = weaponTags.getCompoundTag(key);

                int slot = -1;
                String modname = "";
                if (weapon.hasKey("attachment")) {
                    slot = weapon.getInteger("attachment");
                    NBTTagCompound attach = weaponTagsHandler.getAttachmentBySlot(slot);
                    if (event.getItemStack().getTagCompound() != null) {
                        if (attach.hasKey("display")) {
                            if (attach.getCompoundTag("display").hasKey("Name")) {
                                modname = attach.getCompoundTag("display").getString("Name");
                            }
                        }
                    }
                }
                // MELEE
                if (weapon.hasKey(WeaponTagsHandler.MELEE_KEY)) {

                    NBTTagCompound meleeStats = weapon.getCompoundTag(WeaponTagsHandler.MELEE_KEY);

                    // BASIC INFO
                    String toTooltip = TextFormatting.GRAY + "[Ближнее";
                    String category = meleeStats.getString("category");


                    // DAMAGE/PSYCHDAMAGE/ACCURACY/DEFENCEMOD/SKILL
                    String damageStr = "";
                    String damageExplain = "";
                    if (!(weaponTagsHandler.getWeaponMod(key).equals("0"))) {
                        damageStr += "+" + weaponTagsHandler.getWeaponMod(key);
                        damageExplain = TextFormatting.DARK_RED + "[Урон] ";
                    }
                    String psychdamageStr = "";
                    String psychdamageExplain = "";
                    if (!(Objects.equals(weaponTagsHandler.getWeaponPsychMod(key), "0"))) {
                        psychdamageStr += "+" + weaponTagsHandler.getWeaponPsychMod(key);
                        psychdamageExplain = TextFormatting.LIGHT_PURPLE + "[Психический урон] ";
                    }
                    String accuracyStr = "";
                    String accuracyExplain = "";
                    if (!(Objects.equals(weaponTagsHandler.getWeaponAccuracyMod(key), 0))) {
                        int accuracyinput = weaponTagsHandler.getWeaponAccuracyMod(key);
                        if (accuracyinput > 0) {
                            accuracyStr += "+" + accuracyinput;
                        } else {
                            accuracyStr += accuracyinput;
                        }
                        accuracyExplain = TextFormatting.YELLOW + "[Точность] ";
                    }
                    String defencemodStr = "";
                    String defencemodExplain = "";
                    if (!(Objects.equals(weaponTagsHandler.getWeaponDefenceMod(key), 0))) {
                        int defenceinput = weaponTagsHandler.getWeaponDefenceMod(key);
                        if (defenceinput > 0) {
                            defencemodStr += "+" + defenceinput;
                        } else {
                            defencemodStr += defenceinput;
                        }
                        defencemodExplain = TextFormatting.DARK_GREEN + "[Мод защиты] ";
                    }

                    ///SKILL
                    String skillStrToTooltip = "";
                    String[] someholder;
                    if (meleeStats.getString("skill").equals("подлое")) {
                        skillStrToTooltip = TextFormatting.GREEN + meleeStats.getString("skill") + TextFormatting.GRAY;
                    }
                    if (meleeStats.getString("skill").equals("яростное")) {
                        skillStrToTooltip = TextFormatting.RED + meleeStats.getString("skill") + TextFormatting.GRAY;
                    }
                    if (meleeStats.getString("skill").equals("тайное")) {
                        skillStrToTooltip = TextFormatting.DARK_PURPLE + meleeStats.getString("skill") + TextFormatting.GRAY;
                    }
                    if (meleeStats.getString("skill").equals("бойкое")) {
                        skillStrToTooltip = TextFormatting.AQUA + meleeStats.getString("skill") + TextFormatting.GRAY;
                    }
                    if (meleeStats.getString("skill").equals("подлое+яростное")) {
                        someholder = meleeStats.getString("skill").split("[+]");
                        skillStrToTooltip = TextFormatting.GREEN + someholder[0] + TextFormatting.GRAY + "+" + TextFormatting.RED + someholder[1] + TextFormatting.GRAY;
                    }
                    if (meleeStats.getString("skill").equals("подлое+тайное")) {
                        someholder = meleeStats.getString("skill").split("[+]");
                        skillStrToTooltip = TextFormatting.GREEN + someholder[0] + TextFormatting.GRAY + "+" + TextFormatting.DARK_PURPLE + someholder[1] + TextFormatting.GRAY;
                    }
                    if (meleeStats.getString("skill").equals("подлое+бойкое")) {
                        someholder = meleeStats.getString("skill").split("[+]");
                        skillStrToTooltip = TextFormatting.GREEN + someholder[0] + TextFormatting.GRAY + "+" + TextFormatting.AQUA + someholder[1] + TextFormatting.GRAY;
                    }
                    if (meleeStats.getString("skill").equals("яростное+тайное")) {
                        someholder = meleeStats.getString("skill").split("[+]");
                        skillStrToTooltip = TextFormatting.RED + someholder[0] + TextFormatting.GRAY + "+" + TextFormatting.DARK_PURPLE + someholder[1] + TextFormatting.GRAY;
                    }
                    if (meleeStats.getString("skill").equals("яростное+бойкое")) {
                        someholder = meleeStats.getString("skill").split("[+]");
                        skillStrToTooltip = TextFormatting.RED + someholder[0] + TextFormatting.GRAY + "+" + TextFormatting.AQUA + someholder[1] + TextFormatting.GRAY;
                    }
                    if (meleeStats.getString("skill").equals("тайное+бойкое")) {
                        someholder = meleeStats.getString("skill").split("[+]");
                        skillStrToTooltip = TextFormatting.DARK_PURPLE + someholder[0] + TextFormatting.GRAY + "+" + TextFormatting.AQUA + someholder[1] + TextFormatting.GRAY;
                    }

                    String damageStrToTooltip = "";
                    if (!(damageStr).equals("")) {
                        damageStrToTooltip = TextFormatting.DARK_RED + " [" + damageStr + "]";
                    }
                    String psychdamageStrToTooltip = "";
                    if (!(psychdamageStr.equals(""))) {
                        psychdamageStrToTooltip = TextFormatting.LIGHT_PURPLE + " [" + psychdamageStr + "]";
                    }
                    String accuracyStrToTooltip = "";
                    if (!(accuracyStr.equals(""))) {
                        accuracyStrToTooltip = TextFormatting.YELLOW + " [" + accuracyStr + "]";
                    }
                    String defencemodStrToTooltip = "";
                    if (!(defencemodStr.equals(""))) {
                        defencemodStrToTooltip = TextFormatting.DARK_GREEN + " [" + defencemodStr + "]";
                    }
                    String attacktypeStrToTooltip = "";
                    if (meleeStats.getString("attacktype").equals("физический")) {
                        attacktypeStrToTooltip = TextFormatting.RED + "[физический]";
                    }
                    if (meleeStats.getString("attacktype").equals("психический")) {
                        attacktypeStrToTooltip = TextFormatting.LIGHT_PURPLE + "[психический]";
                    }
                    String reachStrToTooltip = TextFormatting.BLUE + " [" + meleeStats.getInteger("reach") + "]";
                    String reachExplain = TextFormatting.BLUE + "[Дальность] ";
                    // FACTORS
                    NBTTagList factorsList = meleeStats.getTagList("factors", 8);
                    String factors = "";
                    for (int i = 0; i < factorsList.tagCount(); i++) {
                        factors += factorsList.getStringTagAt(i);
                        if (i != factorsList.tagCount() - 1) factors += ", ";
                    }

                    event.getToolTip().add(TextFormatting.DARK_AQUA + "[" + color + key + TextFormatting.DARK_AQUA + "]" +
                            damageStrToTooltip + psychdamageStrToTooltip + accuracyStrToTooltip + defencemodStrToTooltip + reachStrToTooltip);
                    if (sneak) {
                        event.getToolTip().add(damageExplain + psychdamageExplain + accuracyExplain + defencemodExplain + reachExplain);
                    }
                    if (!modname.equals("")) {
                        event.getToolTip().add(TextFormatting.DARK_GRAY + "[Тег модуля " + modname + TextFormatting.DARK_GRAY + "]");
                    } else if (slot != -1) {
                        event.getToolTip().add(TextFormatting.DARK_GRAY + "[Тег модуля " + slot + TextFormatting.DARK_GRAY + "]");
                    }
                    event.getToolTip().add(toTooltip + ", " + skillStrToTooltip + ", " + factors + "]");
                    event.getToolTip().add(TextFormatting.DARK_GRAY + "Способ нанесения урона: " + attacktypeStrToTooltip);

                    if (!(weaponTagsHandler.getWeaponPerk(key).equals(""))) {
                        event.getToolTip().add(TextFormatting.GOLD + "[" + weaponTagsHandler.getWeaponPerk(key) + "]");
                    }

                    //event.getToolTip().add(damageStrToTooltip + " " + rangeStr);
//                    event.getToolTip().add(TextFormatting.GRAY + "[" + factors + "]");
                    if (meleeStats.hasKey("modsplus")) {
                        String modsplus = meleeStats.getString("modsplus");
                        if (!modsplus.isEmpty()) {
                            event.getToolTip().add("§6Дополнительные эффекты:");
                            event.getToolTip().add("[" + modsplus + "]");
                        }
                    }

                    if (meleeStats.hasKey("diff"))
                        event.getToolTip().add("§eСложность: " + getResultAsString(meleeStats.getInteger("diff")));
                    if (meleeStats.hasKey("cost")) {
                        if (meleeStats.getInteger("cost") > 0)
                            event.getToolTip().add("§bСтоимость в мане: " + meleeStats.getInteger("cost"));
                    }
                }

                // RANGED FIREARM

                if (weapon.hasKey(WeaponTagsHandler.FIREARM_KEY)) {

                    NBTTagCompound firearmStats = weapon.getCompoundTag(WeaponTagsHandler.FIREARM_KEY);
                    // BASIC INFO
                    String name = key;
                    if (name.isEmpty()) {
                        name = "Огнестрел";
                    }
                    String toTooltip = color + "[" + name + "]";
                    String damage = firearmStats.getString("damage");
                    String damageExplain = "";
                    if (!(damage.equals("0"))) {
                        damage = TextFormatting.DARK_RED + " [+" + firearmStats.getString("damage") + "]";
                        damageExplain = TextFormatting.DARK_RED + "[Урон] ";
                    } else damage = "";
                    String psychdamage = firearmStats.getString("psychdamage");
                    String psychdamageExplain = "";
                    if (!(psychdamage.equals("0"))) {
                        psychdamage = TextFormatting.LIGHT_PURPLE + " [+" + firearmStats.getString("psychdamage") + "]";
                        psychdamageExplain = TextFormatting.LIGHT_PURPLE + "[Психический урон] ";
                    } else psychdamage = "";
                    String attacktype = firearmStats.getString("attacktype");
                    if (attacktype.equals("физический")) {
                        attacktype = TextFormatting.RED + "[" + firearmStats.getString("attacktype") + "]";
                    }
                    if (attacktype.equals("психический")) {
                        attacktype = TextFormatting.DARK_PURPLE + "[" + firearmStats.getString("attacktype") + "]";
                    }
                    String recoil = firearmStats.getString("recoil");
                    String recoilTurns = firearmStats.getString("recoilTurns");
                    String damagetype = firearmStats.getString("damagetype");
                    if (damagetype.equals("летальный")) {
                        damagetype = TextFormatting.RED + "[" + firearmStats.getString("damagetype") + "]";
                    }
                    if (damagetype.equals("критический")) {
                        damagetype = TextFormatting.GOLD + "[" + firearmStats.getString("damagetype") + "]";
                    }
                    if (damagetype.equals("нелетальный")) {
                        damagetype = TextFormatting.DARK_GRAY + "[" + firearmStats.getString("damagetype") + "]";
                    }
                    String category = firearmStats.getString("category");
                    if (category.equals("прихотливое")) {
                        category = TextFormatting.GOLD + "[" + firearmStats.getString("category") + "]";
                    }
                    String accuracy = String.valueOf(weapon.getInteger("accuracy"));
                    String accuracyExplain = "";
                    if (!(accuracy.equals("0"))) {
                        accuracy = TextFormatting.YELLOW + " [" + weapon.getInteger("accuracy") + "]";
                        accuracyExplain = TextFormatting.YELLOW + "[Точность]";
                    } else
                        accuracy = "";
                    String defenceMod = String.valueOf(weapon.getInteger("defencemod"));
                    String defenceModExplain = "";
                    if (!(defenceMod.equals("0"))) {
                        defenceMod = TextFormatting.GREEN + " [" + weapon.getInteger("defencemod") + "]";
                        defenceModExplain = TextFormatting.GREEN + " [Мод защиты]";
                    } else
                        defenceMod = "";
                    String type = firearmStats.getString("type");
                    String caliber = firearmStats.getString("caliber");
                    String range = TextFormatting.BLUE + "[" + firearmStats.getString("range") + "]";
                    String reload = String.valueOf(firearmStats.getInteger("reload"));
                    boolean serial = (firearmStats.hasKey("serial") && firearmStats.getBoolean("serial"));
                    boolean sdouble = (firearmStats.hasKey("double") && firearmStats.getBoolean("double"));
                    int homemade = 0;
                    type = type.substring(0, 1).toUpperCase() + type.substring(1);
                    ///skill
                    String skillStrToTooltip = "";
                    String[] someholder;
                    if (firearmStats.getString("skill").equals("подлое")) {
                        skillStrToTooltip = TextFormatting.GREEN + firearmStats.getString("skill") + TextFormatting.GRAY;
                    }
                    if (firearmStats.getString("skill").equals("яростное")) {
                        skillStrToTooltip = TextFormatting.RED + firearmStats.getString("skill") + TextFormatting.GRAY;
                    }
                    if (firearmStats.getString("skill").equals("тайное")) {
                        skillStrToTooltip = TextFormatting.DARK_PURPLE + firearmStats.getString("skill") + TextFormatting.GRAY;
                    }
                    if (firearmStats.getString("skill").equals("бойкое")) {
                        skillStrToTooltip = TextFormatting.AQUA + firearmStats.getString("skill") + TextFormatting.GRAY;
                    }
                    if (firearmStats.getString("skill").equals("подлое+яростное")) {
                        someholder = firearmStats.getString("skill").split("[+]");
                        skillStrToTooltip = TextFormatting.GREEN + someholder[0] + TextFormatting.GRAY + "+" + TextFormatting.RED + someholder[1] + TextFormatting.GRAY;
                    }
                    if (firearmStats.getString("skill").equals("подлое+тайное")) {
                        someholder = firearmStats.getString("skill").split("[+]");
                        skillStrToTooltip = TextFormatting.GREEN + someholder[0] + TextFormatting.GRAY + "+" + TextFormatting.DARK_PURPLE + someholder[1] + TextFormatting.GRAY;
                    }
                    if (firearmStats.getString("skill").equals("подлое+бойкое")) {
                        someholder = firearmStats.getString("skill").split("[+]");
                        skillStrToTooltip = TextFormatting.GREEN + someholder[0] + TextFormatting.GRAY + "+" + TextFormatting.AQUA + someholder[1] + TextFormatting.GRAY;
                    }
                    if (firearmStats.getString("skill").equals("яростное+тайное")) {
                        someholder = firearmStats.getString("skill").split("[+]");
                        skillStrToTooltip = TextFormatting.RED + someholder[0] + TextFormatting.GRAY + "+" + TextFormatting.DARK_PURPLE + someholder[1] + TextFormatting.GRAY;
                    }
                    if (firearmStats.getString("skill").equals("яростное+бойкое")) {
                        someholder = firearmStats.getString("skill").split("[+]");
                        skillStrToTooltip = TextFormatting.RED + someholder[0] + TextFormatting.GRAY + "+" + TextFormatting.AQUA + someholder[1] + TextFormatting.GRAY;
                    }
                    if (firearmStats.getString("skill").equals("тайное+бойкое")) {
                        someholder = firearmStats.getString("skill").split("[+]");
                        skillStrToTooltip = TextFormatting.DARK_PURPLE + someholder[0] + TextFormatting.GRAY + "+" + TextFormatting.AQUA + someholder[1] + TextFormatting.GRAY;
                    }
                    if (firearmStats.hasKey("homemade")) homemade = firearmStats.getInteger("homemade");
                    if (firearmStats.hasKey("sProj")) {
                        for (NBTBase ammo : firearmStats.getTagList("sProj", 10)) {
                            NBTTagCompound tagCompound = (NBTTagCompound) ammo;
                            NBTTagCompound proje = (NBTTagCompound) tagCompound.getTag("projectile");
                            String projdamage = proje.getString("damage");
                            String projdamageExplain = "";
                            if (!(projdamage.equals("0"))) {
                                projdamage = TextFormatting.DARK_RED + " [+" + proje.getString("damage") + "]";
                                projdamageExplain = TextFormatting.DARK_RED + "[Урон] ";
                            } else projdamage = "";
                            String projpsychdamage = proje.getString("psychdamage");
                            String projpsychdamageExplain = "";
                            if (!(projpsychdamage.equals("0"))) {
                                projpsychdamage = TextFormatting.LIGHT_PURPLE + " [+" + proje.getString("psychdamage") + "]";
                                projpsychdamageExplain = TextFormatting.LIGHT_PURPLE + "[Психический урон] ";
                            } else projpsychdamage = "";
                            event.getToolTip().add(toTooltip + projdamage + projpsychdamage + accuracy + defenceMod);
                            if (sneak) {
                                event.getToolTip().add(projdamageExplain + projpsychdamageExplain + accuracyExplain + defenceModExplain);
                            }
                            if (!modname.isEmpty()) {
                                event.getToolTip().add(TextFormatting.DARK_GRAY + "[Тег модуля " + modname + TextFormatting.DARK_GRAY + "]");
                            } else if (slot != -1) {
                                event.getToolTip().add(TextFormatting.DARK_GRAY + "[Тег модуля " + slot + TextFormatting.DARK_GRAY + "]");
                            }
                            event.getToolTip().add(TextFormatting.GRAY + "Тип: " + type);
                            event.getToolTip().add(TextFormatting.GRAY + "Калибр: " + caliber);
                            //event.getToolTip().add(TextFormatting.GRAY + type + ", " + caliber);
                            event.getToolTip().add(TextFormatting.GRAY + "Дальность: " + range);
                            event.getToolTip().add(TextFormatting.GRAY + "Используемый навык: " + skillStrToTooltip);
                            event.getToolTip().add(TextFormatting.GRAY + "Категория: " + category);
                            event.getToolTip().add(TextFormatting.GRAY + "Тип атаки: " + attacktype);
                            event.getToolTip().add(TextFormatting.GRAY + "Тип урона: " + damagetype);

                            if (serial) {
                                event.getToolTip().add(TextFormatting.GRAY + "Есть автоматический огонь");
                            }
                            if (sdouble) {
                                event.getToolTip().add(TextFormatting.GRAY + "Доступен двойной выстрел");
                            }
                            if (proje.hasKey("modsplus")) {
                                String modsplus = proje.getString("modsplus");
                                if (!modsplus.isEmpty()) event.getToolTip().add("§6Дополнительные эффекты:");
                                if (!modsplus.isEmpty()) event.getToolTip().add("§6[" + modsplus + "]");
                            }
                            if (proje.hasKey("diff"))
                                event.getToolTip().add("§eСложность: " + getResultAsString(proje.getInteger("diff")));
                            if (proje.hasKey("cost")) {
                                if (proje.getInteger("cost") > 0)
                                    event.getToolTip().add("§bСтоимость в мане: " + proje.getInteger("cost"));
                            }
                        }

                    } else {
                        event.getToolTip().add(toTooltip + damage + psychdamage + accuracy + defenceMod);
                        if (sneak) {
                            event.getToolTip().add(damageExplain + psychdamageExplain + accuracyExplain + defenceModExplain);
                        }
                        if (!modname.equals("")) {
                            event.getToolTip().add(TextFormatting.DARK_GRAY + "[Тег модуля " + modname + TextFormatting.DARK_GRAY + "]");
                        } else if (slot != -1) {
                            event.getToolTip().add(TextFormatting.DARK_GRAY + "[Тег модуля " + slot + TextFormatting.DARK_GRAY + "]");
                        }
                        event.getToolTip().add(TextFormatting.GRAY
                                + " " + type + ", калибр " + caliber);
                        event.getToolTip().add(TextFormatting.GRAY + "Дальность: " + range);
                        event.getToolTip().add(TextFormatting.GRAY + "Используемый навык: " + skillStrToTooltip);
                        if (!reload.equals("0")) {
                            event.getToolTip().add(TextFormatting.GRAY + reload + " хода на смену магазина");
                        }
                        if (!recoil.equals("0")&&!recoil.isEmpty()) {
                            event.getToolTip().add(TextFormatting.GRAY + "Мощность отдачи: " + recoil);
                        }
                        if (!recoilTurns.equals("0")&&!recoilTurns.isEmpty()) {
                            event.getToolTip().add(TextFormatting.GRAY + "Ходов до конца отдачи: " + recoilTurns);
                        }
                        event.getToolTip().add(TextFormatting.GRAY + "Тип урона оружия: " + damagetype);
                        event.getToolTip().add(TextFormatting.GRAY + "Тип атаки: " + attacktype);
                        event.getToolTip().add(TextFormatting.GRAY + "Категория оружия: " + category);
                        if (serial) {
                            event.getToolTip().add(TextFormatting.GRAY + "Есть автоматический огонь");
                        }
                        if (sdouble) {
                            event.getToolTip().add(TextFormatting.GRAY + "Доступен двойной выстрел");
                        }

                        if (homemade > 0) {
                            event.getToolTip().add(TextFormatting.DARK_RED + "Шанс заклинить: " + homemade + "%");
                        }
                        // LOADED AMMO
                        if (weaponTagsHandler.isFirearmLoadedByName(key)) {
                            event.getToolTip().add(TextFormatting.RED + "Заряжено:");
                            for (NBTBase ammo : firearmStats.getTagList(WeaponTagsHandler.CHAMBER, 10)) {
                                NBTTagCompound tagCompound = (NBTTagCompound) ammo;
                                event.getToolTip().add(ammoToString(tagCompound.getTag("projectile")));

                            }
                        } else {
                            event.getToolTip().add(TextFormatting.RED + "Разряжено");
                        }

                        int mod = weaponTagsHandler.getModuleMods();
                        if (mod > 0) {
                            event.getToolTip().add("Суммарный бонус: +" + mod);
                        } else if (mod < 0) {
                            event.getToolTip().add("Суммарный штраф: " + mod);
                        }
                    }

                }

                if (weapon.hasKey("statusEffect")) {
                    // MAGIC

                    String name = key;
                    if (name.isEmpty()) {
                        name = "Эффект";
                    }
                    String toTooltip = color + "[" + name + "]";
                    event.getToolTip().add(toTooltip);
                    NBTTagCompound meleeStats = weapon.getCompoundTag(WeaponTagsHandler.STATUS_KEY);
                    event.getToolTip().add("§bЭффект: " + meleeStats.getString("statusType"));
                    event.getToolTip().add("§eПродолжительность: " + meleeStats.getInteger("turns") + meleeStats.getString("statusEnd"));
                    String context = meleeStats.getString("context");
                    int contextInt = meleeStats.getInteger("contextInt");
                    if (!context.isEmpty() || contextInt != -666) {
                        event.getToolTip().add("§7Контекст: " + context + (contextInt != -666 ? (context.isEmpty() ? contextInt : " " + contextInt) : ""));
                    }
                }

                if (weapon.hasKey("magic")) {
                    // MAGIC
                    NBTTagCompound meleeStats = weapon.getCompoundTag(WeaponTagsHandler.MAGIC_KEY);
                    if (meleeStats.hasKey("diff"))
                        event.getToolTip().add("§eСложность: " + getResultAsString(meleeStats.getInteger("diff")));
                    if (meleeStats.hasKey("cost")) {
                        if (meleeStats.getInteger("cost") > 0)
                            event.getToolTip().add("§bСтоимость в мане: " + meleeStats.getInteger("cost"));
                    }
                    if (meleeStats.hasKey("turns"))
                        event.getToolTip().add("§bХоды подготовки: " + meleeStats.getInteger("turns"));
                    if (meleeStats.hasKey("debuff"))
                        event.getToolTip().add("§cШтраф при подготовке: " + meleeStats.getInteger("debuff"));
                    if (meleeStats.hasKey("defence"))
                        event.getToolTip().add("§2Защита: " + meleeStats.getString("defence"));
                    if (meleeStats.hasKey("range"))
                        event.getToolTip().add("§7Дальность: " + meleeStats.getInteger("range"));
                    if (meleeStats.hasKey("multicast"))
                        event.getToolTip().add("§9Кол-во целей: до " + meleeStats.getInteger("multicast"));
                    if (meleeStats.hasKey("multicast") && meleeStats.hasKey("chain"))
                        event.getToolTip().add("§eЦепь: " + meleeStats.getInteger("chain") + " метров");
                    if (meleeStats.hasKey("multicast") && meleeStats.hasKey("radius"))
                        event.getToolTip().add("§eРадиус: " + meleeStats.getInteger("radius") + " метров");
                    if (meleeStats.hasKey("instant") && meleeStats.getBoolean("instant"))
                        event.getToolTip().add("§bМгновенное использование");
                    if (meleeStats.hasKey("cooldown"))
                        event.getToolTip().add("§1Восстановление: " + meleeStats.getInteger("cooldown"));
                }

                /*if (weapon.hasKey("shield")) {
                    // DEFENCE
                    NBTTagCompound shieldStats = weapon.getCompoundTag("shield");
                    int defence = shieldStats.getInteger("defence");
                    String defenceStr = String.valueOf(defence);
                    event.getToolTip().add(color + "[" + key + "] " + TextFormatting.RED + "[+" + defenceStr + "]");
                    event.getToolTip().add(TextFormatting.GRAY + "[" + shieldStats.getString("category") + ", " +
                            "" + shieldStats.getString("material") + "]");

                    //event.getToolTip().add(TextFormatting.RED + "Защита: [" + defenceStr + "]");
                }*/
                if (weapon.getKeySet().contains("armor")||weapon.getKeySet().contains("shield")||weapon.getKeySet().contains("trinket")) {
                    NBTTagCompound armorStats = null;
                    if (weapon.getKeySet().contains("armor")) {
                        armorStats = weapon.getCompoundTag("armor");
                    }
                    if (weapon.getKeySet().contains("shield")) {
                        armorStats = weapon.getCompoundTag("shield");
                    }
                    if (weapon.getKeySet().contains("trinket")) {
                        armorStats = weapon.getCompoundTag("trinket");
                    }
                    event.getToolTip().add(color + "[" + key.substring(0, 1).toUpperCase() + key.substring(1) + "]");
                    String skillStrToTooltip = "";
                    String[] someholder;
                    if (armorStats.getString("skill").equals("подлое")) {
                        skillStrToTooltip = TextFormatting.GREEN + armorStats.getString("skill") + TextFormatting.GRAY;
                    }
                    if (armorStats.getString("skill").equals("яростное")) {
                        skillStrToTooltip = TextFormatting.RED + armorStats.getString("skill") + TextFormatting.GRAY;
                    }
                    if (armorStats.getString("skill").equals("тайное")) {
                        skillStrToTooltip = TextFormatting.DARK_PURPLE + armorStats.getString("skill") + TextFormatting.GRAY;
                    }
                    if (armorStats.getString("skill").equals("бойкое")) {
                        skillStrToTooltip = TextFormatting.AQUA + armorStats.getString("skill") + TextFormatting.GRAY;
                    }
                    if (armorStats.getString("skill").equals("подлое+яростное")) {
                        someholder = armorStats.getString("skill").split("[+]");
                        skillStrToTooltip = TextFormatting.GREEN + someholder[0] + TextFormatting.GRAY + "+" + TextFormatting.RED + someholder[1] + TextFormatting.GRAY;
                    }
                    if (armorStats.getString("skill").equals("подлое+тайное")) {
                        someholder = armorStats.getString("skill").split("[+]");
                        skillStrToTooltip = TextFormatting.GREEN + someholder[0] + TextFormatting.GRAY + "+" + TextFormatting.DARK_PURPLE + someholder[1] + TextFormatting.GRAY;
                    }
                    if (armorStats.getString("skill").equals("подлое+бойкое")) {
                        someholder = armorStats.getString("skill").split("[+]");
                        skillStrToTooltip = TextFormatting.GREEN + someholder[0] + TextFormatting.GRAY + "+" + TextFormatting.AQUA + someholder[1] + TextFormatting.GRAY;
                    }
                    if (armorStats.getString("skill").equals("яростное+тайное")) {
                        someholder = armorStats.getString("skill").split("[+]");
                        skillStrToTooltip = TextFormatting.RED + someholder[0] + TextFormatting.GRAY + "+" + TextFormatting.DARK_PURPLE + someholder[1] + TextFormatting.GRAY;
                    }
                    if (armorStats.getString("skill").equals("яростное+бойкое")) {
                        someholder = armorStats.getString("skill").split("[+]");
                        skillStrToTooltip = TextFormatting.RED + someholder[0] + TextFormatting.GRAY + "+" + TextFormatting.AQUA + someholder[1] + TextFormatting.GRAY;
                    }
                    if (armorStats.getString("skill").equals("тайное+бойкое")) {
                        someholder = armorStats.getString("skill").split("[+]");
                        skillStrToTooltip = TextFormatting.DARK_PURPLE + someholder[0] + TextFormatting.GRAY + "+" + TextFormatting.AQUA + someholder[1] + TextFormatting.GRAY;
                    }
                    event.getToolTip().add("Используемый навык: " + skillStrToTooltip);
                    String convenience = String.valueOf(weapon.getInteger("convenience"));
                    String convenienceExplain = "";
                    if (!(convenience.equals("0"))) {
                        convenience = TextFormatting.YELLOW + " [" + weapon.getInteger("convenience") + "]";
                        convenienceExplain = TextFormatting.YELLOW + " [Удобство]";
                    } else
                        convenience = "";
                    String defenceMod = String.valueOf(weapon.getInteger("defencemod"));
                    String defenceModExplain = "";
                    if (!(defenceMod.equals("0"))) {
                        defenceMod = TextFormatting.GREEN + " [" + weapon.getInteger("defencemod") + "]";
                        defenceModExplain = TextFormatting.GREEN + " [Мод защиты]";
                    } else
                        defenceMod = "";
                    String armor = weapon.getString("armored");
                    String armorExplain = "";
                    if (!(armor.equals("0") || armor.equals(""))) {
                        armor = TextFormatting.DARK_RED + "[" + weapon.getString("armored") + "]";
                        armorExplain = TextFormatting.DARK_RED + "[Физ-броня]";
                    } else
                        armor = "";
                    String psychdefence = weapon.getString("psychdefence");
                    String psychdefenceExplain = "";
                    if (!(psychdefence.equals("0") || psychdefence.equals(""))) {
                        if (!(armor.equals("0") || armor.equals(""))) {
                            psychdefence = TextFormatting.DARK_PURPLE + " [" + weapon.getString("psychdefence") + "]";
                            psychdefenceExplain = TextFormatting.DARK_PURPLE + " [Псих-броня]";
                        } else {
                            psychdefence = TextFormatting.DARK_PURPLE + "[" + weapon.getString("psychdefence") + "]";
                            psychdefenceExplain = TextFormatting.DARK_PURPLE + "[Псих-броня]";
                        }
                    } else
                        psychdefence = "";
                    event.getToolTip().add(armor + psychdefence + convenience + defenceMod);
                    if (sneak) {
                        event.getToolTip().add(armorExplain + psychdefenceExplain + convenienceExplain + defenceModExplain);
                    }
                }

                if (SkillSaverHandler.hasCompound(event.getItemStack())) {
                    SkillSaverHandler ssv = new SkillSaverHandler(event.getItemStack());
                    NBTTagCompound result = ssv.getFullSkillSaverForWeapon(event.getEntityPlayer().getName(), key);

                    for (String string : result.getKeySet()) {
                        event.getToolTip().add(TextFormatting.GRAY + "[" + string + ": " + result.getString(string) + "]");
                    }
                }
                //event.getToolTip().add("");
            }
        }

        // DURABILITY

        /*boolean durabilitySpace = false;
        WeaponDurabilityHandler weaponDurabilityHandler = new WeaponDurabilityHandler(event.getItemStack());
        if (weaponDurabilityHandler.hasDurabilityDict()) {
            if (!durabilitySpace) {
                event.getToolTip().add("");
                durabilitySpace = true;
            }

            NBTTagCompound durabilityDict = weaponDurabilityHandler.getDurabilityDict();

            int durability = durabilityDict.getInteger("durability");
            int maxDurability = durabilityDict.getInteger("maxDurability");
            int percentageRatio = weaponDurabilityHandler.getPercentageRatio();
            StringBuilder durString = new StringBuilder(TextFormatting.DARK_GRAY + "[Прочность предмета]");

            String color2 = "";
            if (percentageRatio > 75) {
                color2 = "§2";
                durString.append(" §2[ЦЕЛО]");
            } else if (percentageRatio > 50) {
                color2 = "§e";
                durString.append(" §e[ПОЦАРАПАНО]");
            } else if (percentageRatio > 25) {
                color2 = "§7";
                durString.append(" §7[ПОТРЕПАНО]");
            } else if (percentageRatio > 0) {
                color2 = "§c";
                durString.append(" §c[ИЗНОШЕНО]");
            } else if (percentageRatio == 0) {
                color2 = "§4";
                durString.append(" §4[СЛОМАНО]");
            } else if (percentageRatio < 0) {
                color2 = "§8";
                if (durability < -600) {
                    durString.append(" §8[РАЗОБРАНО НА ДЕТАЛИ]");
                } else {
                    durString.append(" §8[РАЗВАЛИЛОСЬ]");
                }
            }
            if (percentageRatio >= 0)
                durString.append(" ").append(color2).append("[").append(durability).append("/").append(maxDurability).append("]");
            event.getToolTip().add(durString.toString());
        }*/

        /*if (event.getItemStack().hasTagCompound() && event.getItemStack().getTagCompound() != null) {
            if (event.getItemStack().getTagCompound().hasKey("attachments")) {
                for (String attach : event.getItemStack().getTagCompound().getCompoundTag("attachments").getKeySet()) {
                    //System.out.println(attach);
                    NBTTagCompound attachment = event.getItemStack().getTagCompound().getCompoundTag("attachments").getCompoundTag(attach);
                    if (attachment.hasKey(DURABILITY_DICT_COMPOUND)) {
                        //System.out.println(DURABILITY_DICT_COMPOUND);
                        String name = "";
                        NBTTagCompound dur = attachment.getCompoundTag(DURABILITY_DICT_COMPOUND);
                        if (attachment.hasKey("display")) {
                            if (attachment.getCompoundTag("display").hasKey("Name")) {
                                name = attachment.getCompoundTag("display").getString("Name");
                            }
                        }
                        if (name.isEmpty()) name = attach;
                        StringBuilder durString = new StringBuilder(TextFormatting.DARK_GRAY + "[Прочность модуля " + name + TextFormatting.DARK_GRAY + "]");
                        int durability = dur.getInteger("durability");
                        int maxDurability = dur.getInteger("maxDurability");
                        float result = (float) durability / maxDurability;
                        int percentageRatio = Math.round(result * 100);
                        String color2 = "";
                        if (percentageRatio > 75) {
                            color2 = "§2";
                            durString.append(" §2[ЦЕЛО]");
                        } else if (percentageRatio > 50) {
                            color2 = "§e";
                            durString.append(" §e[ПОЦАРАПАНО]");
                        } else if (percentageRatio > 25) {
                            color2 = "§7";
                            durString.append(" §7[ПОТРЕПАНО]");
                        } else if (percentageRatio > 0) {
                            color2 = "§c";
                            durString.append(" §c[ИЗНОШЕНО]");
                        } else if (percentageRatio == 0) {
                            color2 = "§4";
                            durString.append(" §4[СЛОМАНО]");
                        } else if (percentageRatio < 0) {
                            color2 = "§8";
                            if (durability < -600) {
                                durString.append(" §8[РАЗОБРАНО НА ДЕТАЛИ]");
                            } else {
                                durString.append(" §8[РАЗВАЛИЛОСЬ]");
                            }
                        }
                        if (percentageRatio >= 0)
                            durString.append(" ").append(color2).append("[").append(durability).append("/").append(maxDurability).append("]");
                        if (!durabilitySpace) {
                            event.getToolTip().add("");
                            durabilitySpace = true;
                        }
                        event.getToolTip().add(durString.toString());
                    }
                }

            }
        }*/

        // WEIGHT

        WeightHandler weightHandler = new WeightHandler(event.getItemStack());
        int weight = weightHandler.getWeightAnything();
        boolean erm = false;
        if (weight != 0) {
            event.getToolTip().add("");
            erm = true;
            event.getToolTip().add("§9Вес: " + weight);
        }

        int psychweight = weightHandler.getPsychWeight();
        if (psychweight != 0) {
            if (!erm) event.getToolTip().add("");
            event.getToolTip().add("§5Психический вес: " + psychweight);
        }

        CostHandler costHandler = new CostHandler(event.getItemStack());
        String cost = costHandler.getCostString();
        if (!cost.isEmpty() && event.getEntityPlayer().isCreative()) {
            event.getToolTip().add("");
            event.getToolTip().add("§eЦена: " + cost);
        }

        TrapHandler trapHandler = new TrapHandler(event.getItemStack());
        String trapdamage = trapHandler.getTrapString();
        if (!trapdamage.isEmpty() && event.getEntityPlayer().isCreative()) {
            event.getToolTip().add("");
            event.getToolTip().add("§4[Ловушка. Урон взрыва: " + trapdamage + "]");
        }
        if (!trapdamage.isEmpty() && !event.getEntityPlayer().isCreative()) {
            event.getToolTip().add("");
            event.getToolTip().add(TextFormatting.DARK_RED + "[Вы попали в ловушку, ГМы оповещены. Ждите]");
            ItemStack itemStack = event.getItemStack();
            if (this.lastItemStack != itemStack){
                Minecraft.getMinecraft().player.sendChatMessage("/dmsggm ГМ, я попался в ловушку в координатах X:" + Math.floor(event.getEntityPlayer().posX) + " Y:" + Math.floor(event.getEntityPlayer().posY) + " Z:" + Math.floor(event.getEntityPlayer().posZ) + " Измерение:" + Minecraft.getMinecraft().player.dimension);
                this.lastItemStack = itemStack;
            }
        }



        EquipmentBuffTagHandler equipmentBuffHandler = new EquipmentBuffTagHandler(event.getItemStack());
        if (equipmentBuffHandler.hasEbTag()) {
            event.getToolTip().add("");
            String[] tags = equipmentBuffHandler.getStringBuffs().split("\\[ТУТ ПЕРЕНОС]");
            for (String tag : tags) {
                event.getToolTip().add(tag);
            }
        }

        if (event.getItemStack().getTagCompound().hasKey("useable")) {
            NBTTagCompound useable = event.getItemStack().getTagCompound().getCompoundTag("useable");
            switch (useable.getString("type")) {
                case "grindstone":
                    event.getToolTip().add("");
                    event.getToolTip().add("§7[Использований точильного камня: " + useable.getInteger("uses") + "]");
                    break;
                case "gunoil":
                    event.getToolTip().add("");
                    event.getToolTip().add("§7[Оружийное масло]");
                    break;
                case "mana":
                    if (useable.hasKey("hidden") && useable.getInteger("hidden") == 1) {
                        event.getToolTip().add("");
                        event.getToolTip().add("§3[Восполняет неизвестное количество маны]");
                    } else {
                        event.getToolTip().add("");
                        event.getToolTip().add("§3[Восполняет §b" + useable.getInteger("regen") + " §3маны]");
                    }
                    break;
                case "energy":
                    if (useable.hasKey("hidden") && useable.getInteger("hidden") == 1) {
                        event.getToolTip().add("");
                        event.getToolTip().add("§6[Восполняет неизвестное количество энергии]");
                    } else {
                        event.getToolTip().add("");
                        event.getToolTip().add("§6[Восполняет §2" + useable.getInteger("regen") + " §6энергии]");
                    }
                    break;
                case "heal":
                    event.getToolTip().add("");
                    event.getToolTip().add("§7[Количество восстанавливаемых ран: " + "§6" + (int) Math.floor((double) useable.getInteger("amount")/2) + " КР" + " §7/ §c" + useable.getInteger("amount") + " ЛР" + " §7/ §2" + useable.getInteger("amount")*2 + " НР" + "§7]");
                    break;
                case "heal2":
                    event.getToolTip().add("");
                    event.getToolTip().add("§7[Количество восстанавливаемых ран: " + "§6" + useable.getInteger("crit") + " КР" + " §7/ §c" + useable.getInteger("leth") + " ЛР" +" §7/ §2" + useable.getInteger("nonleth") + " НР" + "§7]");
                    break;
                case "melee_repair_kit":
                    event.getToolTip().add("");
                    event.getToolTip().add("§7[Набор для ремонта ближнего]");
                    break;
                case "ranged_repair_kit":
                    event.getToolTip().add("");
                    event.getToolTip().add("§7[Набор для ремонта дальнего]");
                    break;
                case "leather_patch":
                    event.getToolTip().add("");
                    event.getToolTip().add("§7[Кожаная заплата]");
                    break;
                case "steel_patch":
                    event.getToolTip().add("");
                    event.getToolTip().add("§7[Стальная заплата]");
                    break;
                case "light_repair_kit":
                    event.getToolTip().add("");
                    event.getToolTip().add("§7[Набор для ремонта легкой брони и щитов]");
                    break;
                case "heavy_repair_kit":
                    event.getToolTip().add("");
                    event.getToolTip().add("§7[Набор для ремонта тяжелой брони и щитов]");
                    break;
                case "stimulator":
                    event.getToolTip().add("");
                    event.getToolTip().add("§3[Стимулятор]");
                    if (event.getEntityPlayer().isCreative()) {
                        event.getToolTip().add("§7[" + (useable.hasKey("trait") ? "Трейт: " + useable.getString("trait") + " " : "") + (useable.hasKey("skills") ? "Скиллы: " + useable.getString("skills") + " Бафф: " + useable.getInteger("buff") + " " : "") + "Ходы: " + useable.getInteger("turns") + " Отходняк: " + useable.getInteger("withdrawal") + " Штраф:" + useable.getInteger("debuff") + "]");
                    }
                    break;
                case "lottery":
                    if (event.getEntityPlayer().isCreative()) {
                        event.getToolTip().add("");
                        if (useable.getString("id").equals("-1")) {
                            event.getToolTip().add("§2[ГМ: чистый билет, можно свободно копировать!!!]");
                        } else {
                            event.getToolTip().add("§4§l[ГМ: использованный билет, НЕЛЬЗЯ копировать!!!]");
                        }
                    }
            }
        }

        if (event.getItemStack().getTagCompound().hasKey("canbeplaced")) {
            event.getToolTip().add("");
            event.getToolTip().add("§2[Блок можно ставить несмотря на лор!]");
        }

        if (event.getItemStack().getTagCompound().hasKey("radiomic")) {
            NBTTagCompound radiomic = event.getItemStack().getTagCompound().getCompoundTag("radiomic");
            event.getToolTip().add("");
            event.getToolTip().add("§7[Радиомикрофон привязан к ID §f" + radiomic.getString("id") + "§7]");
        }

        // GM LORE
        if (GmLoreHandler.hasGmLore(event.getItemStack())) {
            if (!event.getEntityPlayer().isCreative()) return;
            GmLoreHandler gmLoreHandler = new GmLoreHandler(event.getItemStack());

            NBTTagList gmLore = gmLoreHandler.getGmLore();
            event.getToolTip().add("§6[ГМ-лор]");
            //event.getToolTip().add(String.valueOf(gmLore));
            for (int i = 0; i < gmLore.tagCount(); i++) {
                event.getToolTip().add(gmLore.getStringTagAt(i));
            }
        }

        // SKILLSAVER
//        if (SkillSaverHandler.hasCompound(event.getItemStack())) {
//            event.getToolTip().add("");
//            SkillSaverHandler ssv = new SkillSaverHandler(event.getItemStack());
//            NBTTagCompound result = ssv.getFullSkillSaverFor(event.getEntityPlayer().getName());
//
//            for (String string : result.getKeySet()) {
//                event.getToolTip().add(TextFormatting.GRAY + "[" + string + ": " + result.getString(string) + "]");
//            }
//        }

        // GM TAG
        if (GmTagHandler.hasGmTag(event.getItemStack())) {
            GmTagHandler gmTagHandler = new GmTagHandler(event.getItemStack());
            String tag = gmTagHandler.getGmTag();
            event.getToolTip().add("");

            if (gmTagHandler.isHidden() && !event.getEntityPlayer().isCreative()) return;
            String hidden = "";
            if (gmTagHandler.isHidden()) hidden = TextFormatting.GRAY + " (c)";
            event.getToolTip().add(TextFormatting.AQUA + "[" + tag + "]" + hidden);

        }


        // AMMO LORE
        if (AttachmentsHandler.isProjectile(event.getItemStack())) {
            AttachmentsHandler attachmentsHandler = new AttachmentsHandler(event.getItemStack());
            event.getToolTip().add("");

            String caliber = attachmentsHandler.getProjectileOrCreate().getString("caliber");
            String damage = attachmentsHandler.getProjectileOrCreate().getString("damage");
            String damageStr = "";
            if (!damage.equals("0")) damageStr = TextFormatting.DARK_RED + "[" + "+" + damage + "]";

            String psychdamage = attachmentsHandler.getProjectileOrCreate().getString("psychdamage");
            String psychdamageStr = "";
            if (!psychdamage.equals("0")) psychdamageStr = TextFormatting.LIGHT_PURPLE + "[" + "+" + psychdamage + "]";

            String rangemod = attachmentsHandler.getProjectileOrCreate().getString("rangemod");
            String rangemodStr = "";
            if (!rangemod.equals("0")) rangemodStr = TextFormatting.BLUE + " [" + rangemod + "]";

            String mod = attachmentsHandler.getProjectileOrCreate().getString("mod");
            if (!mod.isEmpty()) mod = mod.toLowerCase();

            String holder = "";
            if (!damage.equals("0") && !psychdamage.equals("0")) holder = " ";

            String damagetype = attachmentsHandler.getProjectileOrCreate().getString("damagetype");
            String damagetypeStr = "";
            if (damagetype.equals("летальный")) damagetypeStr = TextFormatting.RED + damagetype;
            if (damagetype.equals("критический")) damagetypeStr = TextFormatting.GOLD + damagetype;
            if (damagetype.equals("нелетальный")) damagetypeStr = TextFormatting.GRAY + damagetype;

            event.getToolTip().add(damageStr + holder + psychdamageStr + rangemodStr);
            event.getToolTip().add("Тип урона: " + damagetypeStr);
            event.getToolTip().add(TextFormatting.GRAY + "Калибр: " + caliber);


            if (attachmentsHandler.getProjectileOrCreate().hasKey("modsplus")) {
                String modsplus = attachmentsHandler.getProjectileOrCreate().getString("modsplus");
                if (!modsplus.isEmpty()) {
                    event.getToolTip().add("§6Дополнительные эффекты:");
                    event.getToolTip().add("§6[" + modsplus + "]");
                }
            }
            if (attachmentsHandler.getProjectileOrCreate().hasKey("diff"))
                event.getToolTip().add("§eСложность: " + getResultAsString(attachmentsHandler.getProjectileOrCreate().getInteger("diff")));
            if (attachmentsHandler.getProjectileOrCreate().hasKey("cost")) {
                if (attachmentsHandler.getProjectileOrCreate().getInteger("cost") > 0)
                    event.getToolTip().add("§bСтоимость в мане: " + attachmentsHandler.getProjectileOrCreate().getInteger("cost"));
            }

            if (attachmentsHandler.getProjectileOrCreate().hasKey("homemade")) {
                int homemade = attachmentsHandler.getProjectileOrCreate().getInteger("homemade");
                if (homemade > 0) {
                    event.getToolTip().add(TextFormatting.DARK_RED + "Шанс заклинить: " + homemade + "%");
                }
            }


        }

        if (WeaponTagsHandler.isMagazine(event.getItemStack())) {
            AttachmentsHandler attachmentsHandler = new AttachmentsHandler(event.getItemStack());
            event.getToolTip().add("");
            event.getToolTip().add(TextFormatting.RED + "[" + attachmentsHandler.getLoadedStuff().tagCount() + "/" +
                    attachmentsHandler.getMagazineSize() +
                    "] " + TextFormatting.GRAY +
                    attachmentsHandler.getMagazineCaliber()
            );

            if (attachmentsHandler.getLoadedStuff().tagCount() > 0) {
                NBTTagCompound upperAmmo = (NBTTagCompound) attachmentsHandler.getLoadedStuff().get(attachmentsHandler.getLoadedStuff().tagCount() - 1);
                event.getToolTip().add(TextFormatting.DARK_RED + "Верхний патрон: " + ammoToString(upperAmmo.getTag("projectile")));
            }
        }

        if (AttachmentsHandler.isModule(event.getItemStack())) {
            AttachmentsHandler attachmentsHandler = new AttachmentsHandler(event.getItemStack());
            event.getToolTip().add("");
            int mod = attachmentsHandler.getMod();
            if (mod > 0) {
                event.getToolTip().add("Процентный бонус модуля: +" + mod);
            } else if (mod < 0) {
                event.getToolTip().add("Процентный штраф модуля: " + mod);
            }
        }


//         event.getToolTip().add("");

    }

    public static String ammoToString(NBTBase ammoNBT) {
        String result = "";
        try {
            NBTTagCompound ammo = (NBTTagCompound) ammoNBT;
            result = ammo.getString("caliber");
            String damage = ammo.getString("damage");

            String damageStr;
            if (!damage.equals("0")) {
                damageStr = TextFormatting.DARK_RED + "[" + "+" + damage + "]";
                result += " " + damageStr;
            }
            else {
                damageStr = TextFormatting.DARK_RED + "[" + 0 + "]";
                result += " " + damageStr;
            }

            String psychdamage = ammo.getString("psychdamage");
            String psychdamageStr = "";
            if (!psychdamage.equals("0")) {
                psychdamageStr = TextFormatting.LIGHT_PURPLE + "[" + "+" + psychdamage + "]";
                result += " " + psychdamageStr;
            }

            String rangemod = ammo.getString("rangemod");
            String rangemodStr = "";
            if (!rangemod.equals("0"))
            {
                rangemodStr = TextFormatting.BLUE + "[" + rangemod + "]";
                result += " " + rangemodStr;
            }
            else {
                rangemodStr = TextFormatting.BLUE + "[" + 0 + "]";
                result += " " + rangemodStr;
            }

            String mod = ammo.getString("mod");
            if (!mod.isEmpty()) mod = mod.toLowerCase();
            result += " " + mod;

            String damagetype = ammo.getString("damagetype");
            if(damagetype.equals("летальный")){
                result += TextFormatting.RED + damagetype;
            }
            if(damagetype.equals("критический")){
                result += TextFormatting.GOLD + damagetype;
            }
            if(damagetype.equals("нелетальный")){
                result += TextFormatting.GRAY + damagetype;
            }


            String modsplus = ammo.getString("modsplus");
            if (!modsplus.isEmpty()) result += " " + TextFormatting.GOLD + modsplus;

        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }


        return result;
    }

}
