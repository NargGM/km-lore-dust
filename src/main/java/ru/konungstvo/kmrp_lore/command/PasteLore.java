package ru.konungstvo.kmrp_lore.command;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.PlayerNotFoundException;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.server.permission.PermissionAPI;
import ru.konungstvo.kmrp_lore.ClientProxy;
import ru.konungstvo.kmrp_lore.helpers.Permissions;

public class PasteLore extends CommandBase {
    @Override
    public String getName() {
        return "pastelore";
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return null;
    }

    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        try {
            return PermissionAPI.hasPermission(getCommandSenderAsPlayer(sender), Permissions.GM.get()) || PermissionAPI.hasPermission(getCommandSenderAsPlayer(sender), Permissions.CM.get());
        } catch (PlayerNotFoundException e) {
            e.printStackTrace();
            return false;
        }
    }



    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        ItemStack item = ((EntityPlayerMP) sender).getHeldItemMainhand();
        item.setTagCompound(ClientProxy.copiedLore);
        ClientProxy.copiedLore = null;


        TextComponentString answer = new TextComponentString("Лор вставлен.");
        answer.getStyle().setColor(TextFormatting.GRAY);
        sender.sendMessage(answer);

    }
}
