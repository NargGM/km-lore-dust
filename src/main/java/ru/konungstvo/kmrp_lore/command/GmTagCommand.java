package ru.konungstvo.kmrp_lore.command;

import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.PlayerNotFoundException;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.JsonToNBT;
import net.minecraft.nbt.NBTException;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.server.permission.PermissionAPI;
import ru.konungstvo.kmrp_lore.helpers.GmTagHandler;
import ru.konungstvo.kmrp_lore.helpers.Permissions;
import ru.konungstvo.kmrp_lore.network.KMPacketHandler;
import ru.konungstvo.kmrp_lore.network.PacketMessage;

import java.awt.*;
import java.util.Arrays;

public class GmTagCommand extends CommandBase {
    public static final String NAME = "gmtag";
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return null;
    }

    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        return ((EntityPlayerSP) sender).isCreative();
    }


    @Override
    @SideOnly(Side.CLIENT)
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        TextComponentString result;
        TextComponentString errorStr;
        ItemStack itemStack = ((EntityPlayerSP) sender).getHeldItemMainhand();
        GmTagHandler handler = new GmTagHandler(itemStack);
        boolean hadTag = GmTagHandler.hasGmTag(itemStack);

        if (args.length == 0) {
            if (!hadTag) {
                result = new TextComponentString(String.format("У этого предмета нет тега."));
                result.getStyle().setColor(TextFormatting.GOLD);
                sender.sendMessage(result);
                return;

            }
            PacketMessage packetMessage = new PacketMessage(handler.getGmTag());
            KMPacketHandler.INSTANCE.sendToServer(packetMessage);
            /*
            result = new TextComponentString("Тут хелп");
            result.getStyle().setColor(TextFormatting.GOLD);
            sender.sendMessage(result);

             */

        } else if (args[0].equals("set")) {
            // PARSE JSON AND TRANSFORM TO NBT, THEN SET
            String tagString = String.join(" ", Arrays.copyOfRange(args, 1, args.length));
            System.out.println(tagString);
            NBTTagCompound tagCompound;
            try {
                tagCompound = JsonToNBT.getTagFromJson(tagString);
            } catch (NBTException e) {
                TextComponentString error = new TextComponentString(e.toString());
                error.getStyle().setColor(TextFormatting.RED);
                sender.sendMessage(error);
                return;
            }
            handler.setGmTag(tagCompound);


            result = new TextComponentString(String.format("Тег %s успешно добавлен.", handler.getGmTag()));
            result.getStyle().setColor(TextFormatting.GOLD);
            sender.sendMessage(result);
        } else if (args[0].equals("remove") || args[0].equals("delete") || args[0].equals("del")) {
            if (!hadTag) {
                result = new TextComponentString(String.format("У этого предмета нет тега."));
                result.getStyle().setColor(TextFormatting.GOLD);
                sender.sendMessage(result);
                return;

            }
            String tag = handler.getGmTag();
            handler.removeTag();
            result = new TextComponentString(String.format("Тег %s удалён", tag));
            result.getStyle().setColor(TextFormatting.GOLD);
            sender.sendMessage(result);
        }

    }
}
