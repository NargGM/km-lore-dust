package ru.konungstvo.kmrp_lore.command;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.PlayerNotFoundException;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.JsonToNBT;
import net.minecraft.nbt.NBTException;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.server.permission.PermissionAPI;
import ru.konungstvo.kmrp_lore.helpers.GmLoreHandler;
import ru.konungstvo.kmrp_lore.helpers.Permissions;

import java.util.Arrays;

public class ModuleCommand extends CommandBase {
    @Override
    public String getName() {
        return "module";
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return null;
    }

    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        try {
            return PermissionAPI.hasPermission(getCommandSenderAsPlayer(sender), Permissions.GM.get());
        } catch (PlayerNotFoundException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        if (args.length == 0) {
            //TODO
        }

        ItemStack item = ((EntityPlayerMP) sender).getHeldItemMainhand();
        String tagString = String.join(" ", Arrays.copyOfRange(args, 1, args.length));
        NBTTagCompound moduleLore;
        try {
            moduleLore = JsonToNBT.getTagFromJson(tagString);
        } catch (NBTException e) {
            TextComponentString error = new TextComponentString(e.toString());
            error.getStyle().setColor(TextFormatting.RED);
            sender.sendMessage(error);
            return;
        }
        if (item.getTagCompound() == null) {
            NBTTagCompound tagCompound = new NBTTagCompound();
            item.setTagCompound(tagCompound);
        }
        item.getTagCompound().merge(moduleLore);



    }
}
