package ru.konungstvo.kmrp_lore.command;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.PlayerNotFoundException;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.server.permission.PermissionAPI;
import ru.konungstvo.kmrp_lore.helpers.GmLoreHandler;
import ru.konungstvo.kmrp_lore.helpers.ItemLoreHandler;
import ru.konungstvo.kmrp_lore.helpers.Permissions;

import java.util.Arrays;

public class GmLoreCommand extends CommandBase {
    @Override
    public String getName() {
        return "gmlore";
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return null;
    }

    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        try {
            return PermissionAPI.hasPermission(getCommandSenderAsPlayer(sender), Permissions.GM.get());
        } catch (PlayerNotFoundException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        if (args.length == 0) {
            //TODO
        }

        ItemStack item = ((EntityPlayerMP) sender).getHeldItemMainhand();
        GmLoreHandler itemLoreHandler = new GmLoreHandler(item);

        String lore = String.join(" ", Arrays.copyOfRange(args, 1, args.length));
        if (!lore.contains("&")) {
            lore = "&f" + lore;
        }

        switch (args[0]) {
            case "set":
                try {
                    itemLoreHandler.setGmLore(args);
                } catch (LoreException e) {
                    TextComponentString error = new TextComponentString(e.toString());
                    error.getStyle().setColor(TextFormatting.RED);
                    sender.sendMessage(error);
                }
                return;

            case "add":
                itemLoreHandler.addGmLore(lore);
                return;

            case "purge":
                try {
                    itemLoreHandler.purgeGmLore();
                } catch (LoreException e) {
                    TextComponentString error = new TextComponentString(e.toString());
                    error.getStyle().setColor(TextFormatting.RED);
                    sender.sendMessage(error);
                }
                return;

            case "del":
                int position;
                try {
                    position = Integer.parseInt(args[1]);
                } catch (NumberFormatException e) {
                    TextComponentString answer = new TextComponentString("После /dellore нужно указать номер строки числом.");
                    answer.getStyle().setColor(TextFormatting.RED);
                    sender.sendMessage(answer);
                    return;
                }
                try {
                    itemLoreHandler.delGmLore(position);
                } catch (LoreException e) {
                    TextComponentString error = new TextComponentString(e.toString());
                    error.getStyle().setColor(TextFormatting.RED);
                    sender.sendMessage(error);
                }
                return;
            case "new":
                try {
                    itemLoreHandler.purgeGmLore(true);
                } catch (LoreException e) {
                    TextComponentString error = new TextComponentString(e.toString());
                    error.getStyle().setColor(TextFormatting.RED);
                    sender.sendMessage(error);
                }
                itemLoreHandler.addGmLore(lore);
                return;
        }
    }
}
