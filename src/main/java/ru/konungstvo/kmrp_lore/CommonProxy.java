package ru.konungstvo.kmrp_lore;

import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;
import net.minecraftforge.fml.common.eventhandler.EventPriority;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.server.FMLServerHandler;
import net.minecraftforge.server.permission.DefaultPermissionLevel;
import net.minecraftforge.server.permission.PermissionAPI;
import org.apache.logging.log4j.Logger;
import ru.konungstvo.kmrp_lore.command.*;
import ru.konungstvo.kmrp_lore.network.KMPacketHandler;
import ru.konungstvo.kmrp_lore.network.PacketMessage;
import ru.konungstvo.kmrp_lore.network.PacketMessageHandler;

import static net.minecraftforge.common.MinecraftForge.EVENT_BUS;

public class CommonProxy {
    private static Logger logger;

    @EventHandler
    public void preInit(FMLPreInitializationEvent event) {
        logger = event.getModLog();
    }

    @EventHandler
    public void init(FMLInitializationEvent event) {

    }

    @Mod.EventHandler
    public void startServer(FMLServerStartingEvent event) {
        System.out.println("KMSystem [FORGE] is starting!");
        EVENT_BUS.register(this);
        KMPacketHandler.INSTANCE.registerMessage(PacketMessageHandler.class, PacketMessage.class, 1, Side.SERVER);

        PermissionAPI.registerNode("km.gm", DefaultPermissionLevel.OP, "Game Master permission");

        event.registerServerCommand(new RenameCommand());
        event.registerServerCommand(new AddloreCommand());
        event.registerServerCommand(new PurgeloreCommand());
        event.registerServerCommand(new SetloreCommand());
        event.registerServerCommand(new DelloreCommand());
        event.registerServerCommand(new NewloreCommand());
        event.registerServerCommand(new InloreCommand());

        event.registerServerCommand(new GmLoreCommand());
        event.registerServerCommand(new WeaponTagsCommand());
        event.registerServerCommand(new EquipmentBuffExecutor());
        event.registerServerCommand(new SetWeightCommand());
        event.registerServerCommand(new SetCostCommand());
        event.registerServerCommand(new SetTrapCommand());
        event.registerServerCommand(new WeaponDurabilityCommand());
        event.registerServerCommand(new AttachmentsCommand());
        event.registerServerCommand(new DebugCommand());
        event.registerServerCommand(new ModuleCommand());
        event.registerServerCommand(new BoltCommand());
        event.registerServerCommand(new ConfirmLore());

        event.registerServerCommand(new CopyLore());
        event.registerServerCommand(new PasteLore());
        event.registerServerCommand(new NRPCommand());

        event.registerServerCommand(new ArmorTagCommand());


        //NetworkRegistry.INSTANCE.registerGuiHandler(this, new GuiHandler());

    }


    @SubscribeEvent(priority = EventPriority.LOWEST)
    public void onPlayerJoin(PlayerEvent.PlayerLoggedInEvent event) {
        event.player.openGui(this, 0, event.player.world,
                (int) event.player.posX, (int) event.player.posY, (int) event.player.posZ);
    }


}
