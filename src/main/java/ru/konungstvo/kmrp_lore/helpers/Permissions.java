package ru.konungstvo.kmrp_lore.helpers;

public enum Permissions {
    GM("km.gm"),
    CM("km.craft");


    private String perm;

    public String get() {
        return perm;
    }

    Permissions(String s) {
        this.perm = s;

    }
}
