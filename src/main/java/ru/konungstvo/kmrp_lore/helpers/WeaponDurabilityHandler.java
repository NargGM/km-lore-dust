package ru.konungstvo.kmrp_lore.helpers;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagString;
import ru.konungstvo.kmrp_lore.command.LoreException;
import scala.Int;

import java.util.ArrayList;
import java.util.List;


public class WeaponDurabilityHandler extends WeaponTagsHandler {
    public static final String COMMON_DURABILITY_COMPOUND = "commonDurability";
    public static final String DURABILITY_DICT_COMPOUND = "durabilityDict";

    public WeaponDurabilityHandler(ItemStack itemStack) {
        super(itemStack);
    }


    public NBTTagCompound getDurabilityDict() {
        if(itemStack.getTagCompound().getCompoundTag(DURABILITY_DICT_COMPOUND) == null) return null;
        return itemStack.getTagCompound().getCompoundTag(DURABILITY_DICT_COMPOUND);
    }

    public boolean hasDurabilityDict() {
        if (this.itemStack.getTagCompound() == null) {
            return false;
        }
        return this.itemStack.getTagCompound().hasKey(DURABILITY_DICT_COMPOUND);
    }
    public boolean hasDurabilityForTag(String weapon) {
        if (this.itemStack.getTagCompound() == null) {
            return false;
        }
        NBTTagCompound weaponTag = getWeaponTags().getCompoundTag(weapon);
        if(weaponTag.hasKey("attachment")) {
            int slot = weaponTag.getInteger("attachment");
            NBTTagCompound attach = getAttachmentBySlot(slot);
            if(attach.hasKey(DURABILITY_DICT_COMPOUND)) {
                return true;
            }
        }
        return this.itemStack.getTagCompound().hasKey(DURABILITY_DICT_COMPOUND);
    }

    public boolean hasDurabilityExactlyForTag(String weapon) {
        if (this.itemStack.getTagCompound() == null) {
            return false;
        }
        NBTTagCompound weaponTag = getWeaponTags().getCompoundTag(weapon);
        if(weaponTag.hasKey("attachment")) {
            int slot = weaponTag.getInteger("attachment");
            NBTTagCompound attach = getAttachmentBySlot(slot);
            if(attach.hasKey(DURABILITY_DICT_COMPOUND)) {
                return true;
            }
        }
        return false;
    }

    public NBTTagCompound getDurabilityExactlyForTag(String weapon) {
        NBTTagCompound attach = getAttachmentByWeaponName(weapon);
        if (attach == null) return null;
        if (!attach.hasKey(DURABILITY_DICT_COMPOUND)) return null;
        return attach.getCompoundTag(DURABILITY_DICT_COMPOUND);
    }

    public int getPercentageRatioForTag(String weapon) {
        NBTTagCompound durabilityDict;
        NBTTagCompound attach = getAttachmentByWeaponName(weapon);
        if(attach != null && attach.hasKey(DURABILITY_DICT_COMPOUND)) {
            durabilityDict = attach.getCompoundTag(DURABILITY_DICT_COMPOUND);
        } else {
            durabilityDict = getDurabilityDict();
        }

        int durability = durabilityDict.getInteger("durability");
        int maxDurability = durabilityDict.getInteger("maxDurability");
        float result = (float) durability / maxDurability;
        return Math.round(result * 100);
    }

    public String getToolTipByPercentage(int percentageRatio) {
        return null;
    }

    public void takeAwayDurabilityForTag(String weapon, int loss) {
        if(!hasDurabilityForTag(weapon)) return;
        NBTTagCompound durabilityDict;
        NBTTagCompound attach = getAttachmentByWeaponName(weapon);
        if(attach != null && attach.hasKey(DURABILITY_DICT_COMPOUND)) {
            durabilityDict = attach.getCompoundTag(DURABILITY_DICT_COMPOUND);
        } else {
            durabilityDict = getDurabilityDict();
        }

        int durability = durabilityDict.getInteger("durability");
        int newDurability = durability - loss;
        if (newDurability < 0) {
            if (Math.random() != 0.0) newDurability = 0;
        }
        //durabilityDict.setInteger("durability", newDurability);
        //getWeaponDurabilityDict(weapon).merge(durabilityDict);
//        tagCompound.getCompoundTag(COMPOUND_NAME).getCompoundTag(weapon).getCompoundTag("durabilityDict").setInteger("durability", newDurability);
        System.out.println("Old durability: " + durabilityDict.toString());
        durabilityDict.setInteger("durability", newDurability);
        System.out.println("New durability: " + durabilityDict.toString());
        System.out.println("New durability with debuff: " + durabilityDict.toString());
    }

//    public void countDebuff(String weapon) {
//        NBTTagCompound durabilityDict = getWeaponDurabilityDict(weapon);
//
//        int durability = durabilityDict.getInteger("durability");
//        int maxDurability = durabilityDict.getInteger("maxDurability");
//        float result = (float) durability / maxDurability;
//        int percentageRatio = Math.round(result * 100);
//
//        if (percentageRatio > 75) {
//            durabilityDict.setString("debuffLabel", "");
//        } else if (percentageRatio > 50) {
//            durabilityDict.setString("debuffLabel", "ПОЦАРАПАНО");
//        } else if (percentageRatio > 25) {
//            durabilityDict.setString("debuffLabel", "ПОТРЕПАНО");
//        } else if (percentageRatio > 0) {
//            durabilityDict.setString("debuffLabel", "СНОШЕНО");
//        } else if (percentageRatio == 0) {
//            durabilityDict.setString("debuffLabel", "СЛОМАНО");
//        } else if (percentageRatio < 0) {
//            durabilityDict.setString("debuffLabel", "РАЗВАЛИЛОСЬ");
//        }
//    }

    public void takeAwayDurability() {
        takeAwayDurability(1);
    }

    public void takeAwayDurability(int loss) {
        if(!hasDurabilityDict()) return;
        NBTTagCompound durabilityDict = getDurabilityDict();

        int durability = durabilityDict.getInteger("durability");
        int newDurability = durability - loss;
        if (newDurability < 0) {
            if (Math.random() != 0.0) newDurability = 0;
        }
        //durabilityDict.setInteger("durability", newDurability);
        //getWeaponDurabilityDict(weapon).merge(durabilityDict);
//        tagCompound.getCompoundTag(COMPOUND_NAME).getCompoundTag(weapon).getCompoundTag("durabilityDict").setInteger("durability", newDurability);
        System.out.println("Old durability: " + durabilityDict.toString());
        durabilityDict.setInteger("durability", newDurability);
        System.out.println("New durability: " + durabilityDict.toString());
        System.out.println("New durability with debuff: " + durabilityDict.toString());
    }

    public void refillDurability(int gain) {
        NBTTagCompound durabilityDict = getDurabilityDict();
        int durability = durabilityDict.getInteger("durability");
        int maxDurability = durabilityDict.getInteger("maxDurability");
        int newDurability = durability + gain;
        if (newDurability > maxDurability) newDurability = maxDurability;
        durabilityDict.setInteger("durability", newDurability);
        //tagCompound.getCompoundTag(COMPOUND_NAME).getCompoundTag(weapon).getCompoundTag("durabilityDict").merge(durabilityDict);
        durabilityDict.merge(durabilityDict);
    }

    public void setDurability(int newdurability) {
        NBTTagCompound durabilityTags = getDurabilityTagsOrCreate();
        durabilityTags.setInteger("durability", newdurability);
        if (!durabilityTags.hasKey("maxDurability")) durabilityTags.setInteger("maxDurability", newdurability);
        durabilityTags.merge(durabilityTags);
        tagCompound.setTag(DURABILITY_DICT_COMPOUND, durabilityTags);
    }

    public int getPercentageRatio() {
        NBTTagCompound durabilityDict = getDurabilityDict();
        int durability = durabilityDict.getInteger("durability");
        int maxDurability = durabilityDict.getInteger("maxDurability");
        float result = (float) durability / maxDurability;
        return Math.round(result * 100);
    }

    public int getDurability() {
        NBTTagCompound durabilityDict = getDurabilityDict();
        return durabilityDict.getInteger("durability");
    }

    public int getMaxDurability() {
        NBTTagCompound durabilityDict = getDurabilityDict();
        return durabilityDict.getInteger("maxDurability");
    }

    public NBTTagCompound getDurabilityTagsOrCreate() {
        if (tagCompound.hasKey(DURABILITY_DICT_COMPOUND)) {
            return tagCompound.getCompoundTag(DURABILITY_DICT_COMPOUND);
        }
        NBTTagCompound durabilityTags = new NBTTagCompound();
        this.tagCompound.setTag(DURABILITY_DICT_COMPOUND, durabilityTags);
        return durabilityTags;
    }

    public void setMaxDurability(int newdurability) {
        NBTTagCompound durabilityTags = getDurabilityTagsOrCreate();
        durabilityTags.setInteger("maxDurability", newdurability);
        if (!durabilityTags.hasKey("durability")) durabilityTags.setInteger("durability", newdurability);
        durabilityTags.merge(durabilityTags);
        //weapontags.setTag(weapon, weapontag);
        tagCompound.setTag(DURABILITY_DICT_COMPOUND, durabilityTags);
    }

//    public String getDebuff(String weapon) {
//        NBTTagCompound durabilityDict = getWeaponDurabilityDict(weapon);
//        return durabilityDict.getString("debuff");
//    }
//
//    public String getDebuffLabel(String weapon) {
//        NBTTagCompound durabilityDict = getWeaponDurabilityDict(weapon);
//        return durabilityDict.getString("debuffLabel");
//    }
}
