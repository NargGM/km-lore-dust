package ru.konungstvo.kmrp_lore.helpers;

public enum SubcompEq {

    SUBCOMPOUND_COMMON("common"),
    SUBCOMPOUND_EQUIPMENT("equipment"),
    SUBCOMPOUND_INVENTORY("inventory");


    private String desc;
    SubcompEq(String desc) {
        this.desc = desc;
    }

    @Override
    public String toString() {
        return desc;
    }
}