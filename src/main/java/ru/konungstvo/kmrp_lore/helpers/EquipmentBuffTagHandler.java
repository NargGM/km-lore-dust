package ru.konungstvo.kmrp_lore.helpers;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagString;

public class EquipmentBuffTagHandler extends LoreHandler {

    public EquipmentBuffTagHandler(ItemStack itemStack) { super(itemStack); }
    public static final String EQUIPMENT_COMPOUND = "equipmentbuff";
    public static final String SUBCOMPOUND_COMMON = "common";
    public static final String SUBCOMPOUND_EQUIPMENT = "equipment";
    public static final String SUBCOMPOUND_INVENTORY = "inventory";

    public NBTTagCompound getEquipmentBuffTagOrCreate() {
        if (tagCompound.hasKey(EQUIPMENT_COMPOUND)) {
            return tagCompound.getCompoundTag(EQUIPMENT_COMPOUND);
        }
        NBTTagCompound ebTag = new NBTTagCompound();
        this.tagCompound.setTag(EQUIPMENT_COMPOUND, ebTag);
        return ebTag;
    }

    public NBTTagCompound getSubCompoundTagOrCreate(SubcompEq se) {
        NBTTagCompound tg = getEquipmentBuffTagOrCreate();
        if (tg.hasKey(se.toString())) {
            return tg.getCompoundTag(se.toString());
        }
        NBTTagCompound ebTag = new NBTTagCompound();
        tg.setTag(se.toString(), ebTag);
        return ebTag;
    }

    public void setBuff(SubcompEq se, String skill, int level) {
        NBTTagCompound tc = getSubCompoundTagOrCreate(se);
        tc.setInteger(skill, level);
    }

    public void setBuff(SubcompEq se, String skill, String level) {
        NBTTagCompound tc = getSubCompoundTagOrCreate(se);
        tc.setString(skill, level);
    }

    public String getStringBuffs() {
        StringBuilder well = new StringBuilder();
        NBTTagCompound tc = getEquipmentBuffTagOrCreate();
        if (hasEbTag(SubcompEq.SUBCOMPOUND_COMMON)) {
            NBTTagCompound tg = getSubCompoundTagOrCreate(SubcompEq.SUBCOMPOUND_COMMON);
            well.append("Баффы:[ТУТ ПЕРЕНОС]");
            for (String key : tg.getKeySet()) {
                if (tg.getTagId(key) == 3) {
                    well.append(key.substring(0, 1).toUpperCase()).append(key.substring(1)).append(" ").append(tg.getInteger(key)).append("[ТУТ ПЕРЕНОС]");
                } else {
                    well.append(key.substring(0, 1).toUpperCase()).append(key.substring(1)).append(" ").append(tg.getString(key)).append("[ТУТ ПЕРЕНОС]");
                }
            }
        }
        if (hasEbTag(SubcompEq.SUBCOMPOUND_EQUIPMENT)) {
            NBTTagCompound tg = getSubCompoundTagOrCreate(SubcompEq.SUBCOMPOUND_EQUIPMENT);
            well.append("Баффы (в снаряжении):[ТУТ ПЕРЕНОС]");
            for (String key : tg.getKeySet()) {
                if (tg.getTagId(key) == 3) {
                    well.append(key.substring(0, 1).toUpperCase()).append(key.substring(1)).append(" ").append(tg.getInteger(key)).append("[ТУТ ПЕРЕНОС]");
                } else {
                    well.append(key.substring(0, 1).toUpperCase()).append(key.substring(1)).append(" ").append(tg.getString(key)).append("[ТУТ ПЕРЕНОС]");
                }
            }
        }
        if (hasEbTag(SubcompEq.SUBCOMPOUND_INVENTORY)) {
            NBTTagCompound tg = getSubCompoundTagOrCreate(SubcompEq.SUBCOMPOUND_INVENTORY);
            well.append("Баффы (в инвентаре):[ТУТ ПЕРЕНОС]");
            for (String key : tg.getKeySet()) {
                if (tg.getTagId(key) == 3) {
                    well.append(key.substring(0, 1).toUpperCase()).append(key.substring(1)).append(" ").append(tg.getInteger(key)).append("[ТУТ ПЕРЕНОС]");
                } else {
                    well.append(key.substring(0, 1).toUpperCase()).append(key.substring(1)).append(" ").append(tg.getString(key)).append("[ТУТ ПЕРЕНОС]");
                }
            }
        }
        return well.toString().trim();
    }

    public int getBuff(SubcompEq se, String skill) {
        if (!hasEbTag(se)) return 0;
        NBTTagCompound tg = getSubCompoundTagOrCreate(se);
        if (tg.hasKey(skill)) {
            tg.getInteger(skill);
            if (tg.getTagId(skill) == 3) {
                return tg.getInteger(skill) * 100;
            } else {
                return Integer.parseInt(tg.getString(skill).replace("%", ""));
            }
        }
        return 0;
    }

    public int getEqBuff(String skill) {
        int buff = 0;
        if (hasEbTag(SubcompEq.SUBCOMPOUND_EQUIPMENT)) {
            NBTTagCompound tg = getSubCompoundTagOrCreate(SubcompEq.SUBCOMPOUND_EQUIPMENT);
            if (tg.hasKey(skill)) {
                if (tg.getTagId(skill) == 3) {
                    buff += tg.getInteger(skill) * 100;
                } else {
                    buff += Integer.parseInt(tg.getString(skill).replace("%", ""));
                }
            }
        }
        if (hasEbTag(SubcompEq.SUBCOMPOUND_COMMON)) {
            NBTTagCompound tg = getSubCompoundTagOrCreate(SubcompEq.SUBCOMPOUND_COMMON);
            if (tg.hasKey(skill)) {
                if (tg.getTagId(skill) == 3) {
                    buff += tg.getInteger(skill) * 100;
                } else {
                    buff += Integer.parseInt(tg.getString(skill).replace("%", ""));
                }
            }
        }
        return buff;
    }

    public int getInvBuff(String skill) {
        int buff = 0;
        if (hasEbTag(SubcompEq.SUBCOMPOUND_INVENTORY)) {
            NBTTagCompound tg = getSubCompoundTagOrCreate(SubcompEq.SUBCOMPOUND_INVENTORY);
            if (tg.hasKey(skill)) {
                if (tg.getTagId(skill) == 3) {
                    buff += tg.getInteger(skill) * 100;
                } else {
                    buff += Integer.parseInt(tg.getString(skill).replace("%", ""));
                }
            }
        }
        if (hasEbTag(SubcompEq.SUBCOMPOUND_COMMON)) {
            NBTTagCompound tg = getSubCompoundTagOrCreate(SubcompEq.SUBCOMPOUND_COMMON);
            if (tg.hasKey(skill)) {
                if (tg.getTagId(skill) == 3) {
                    buff += tg.getInteger(skill) * 100;
                } else {
                    buff += Integer.parseInt(tg.getString(skill).replace("%", ""));
                }
            }
        }
        return buff;
    }

    public static boolean hasEbTag(ItemStack itemStack) {
        if (itemStack.getTagCompound() == null) {
            return false;
        }
        return itemStack.getTagCompound().hasKey(EQUIPMENT_COMPOUND);
    }

    public boolean hasEbTag() {
        if (this.itemStack.getTagCompound() == null) {
            return false;
        }
        return this.itemStack.getTagCompound().hasKey(EQUIPMENT_COMPOUND);
    }

    public boolean hasEbTag(SubcompEq se) {
        if (this.itemStack.getTagCompound() == null) {
            return false;
        }
        if (itemStack.getTagCompound().hasKey(EQUIPMENT_COMPOUND)) {
            return itemStack.getTagCompound().getCompoundTag(EQUIPMENT_COMPOUND).hasKey(se.toString());
        }
        return false;
    }

    public NBTTagCompound getEbTag(ItemStack itemStack) {
        if (itemStack.getTagCompound() != null) {
            if (itemStack.getTagCompound().hasKey(EQUIPMENT_COMPOUND)) {
                return itemStack.getTagCompound().getCompoundTag(EQUIPMENT_COMPOUND);
            }
        }
        return null;
    }

    public NBTTagCompound getEbSubTag(ItemStack itemStack, SubcompEq se) {
        if (itemStack.getTagCompound() != null) {
            if (itemStack.getTagCompound().hasKey(EQUIPMENT_COMPOUND)) {
                if (itemStack.getTagCompound().getCompoundTag(EQUIPMENT_COMPOUND).hasKey(se.toString())) {
                    return itemStack.getTagCompound().getCompoundTag(EQUIPMENT_COMPOUND).getCompoundTag(se.toString());
                }
            }
        }
        return null;
    }

    public NBTTagCompound getEbTag() {
        if (tagCompound.hasKey(EQUIPMENT_COMPOUND)) {
            return tagCompound.getCompoundTag(EQUIPMENT_COMPOUND);
        }
        return null;
    }

    public void removeEbTag() {
        if (tagCompound.hasKey(EQUIPMENT_COMPOUND)) {
            tagCompound.removeTag(EQUIPMENT_COMPOUND);
        }
    }

    public void setEbTag(String channel) {
        if (tagCompound.hasKey(EQUIPMENT_COMPOUND)) {
            tagCompound.getCompoundTag(EQUIPMENT_COMPOUND).setTag("channel", new NBTTagString(channel));
        }
    }

}
