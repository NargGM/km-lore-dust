package ru.konungstvo.kmrp_lore.helpers;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.*;
import ru.konungstvo.kmrp_lore.command.LoreException;

import java.util.ArrayList;
import java.util.List;

import static net.minecraft.item.Item.getIdFromItem;


public class WeaponTagsHandler extends LoreHandler {
    public static final String DAMAGE_KEY = "damage";
    public static final String PSYCHDAMAGE_KEY = "psychdamage";

    public static final String ACCURACY_KEY = "accuracy";
    public static final String DEFENCEMOD_KEY = "defencemod";
    public static final String REACH_KEY = "reach";
    public static final String MELEE_KEY = "melee";
    public static final String PERK_KEY = "perk";
    public static final String STATUS_KEY = "statusEffect";
    public static final String MAGIC_KEY = "magic";
    public static final String FIREARM_KEY = "rangedFirearm";

    public static final String COMPOUND_NAME = "weapontags";

    public static final String[] WEAPON_KEYS = {"melee", "ranged", "throwing", "rangedFirearm"};
    public static final String[] STATUS_KEYS = {"statusEffect"};
    public static final String[] SHIELD_KEYS = {"shield"};
    public static final String[] UNARMED_KEYS = {"knuckle", "shield"};
    public static final String[] ARMOR_KEYS = {"armor"};
    public static final String DEFAULT_WEAPON = "default";

    // FIREARM STUFF
    public static final String CHAMBER = "loadedProjectilesList";
    public static final String CAPACITY = "capacity";

    public WeaponTagsHandler(ItemStack itemStack) {
        super(itemStack);
    }

    public NBTTagCompound getWeaponTagsOrCreate() {
        if (tagCompound.hasKey(COMPOUND_NAME)) {
            return tagCompound.getCompoundTag(COMPOUND_NAME);
        }
        NBTTagCompound weapontags = new NBTTagCompound();
        this.tagCompound.setTag(COMPOUND_NAME, weapontags);
        return weapontags;
    }

    public static boolean hasWeaponTags(ItemStack itemStack) {
        if (itemStack.getTagCompound() == null) {
            return false;
        }
        return itemStack.getTagCompound().hasKey(COMPOUND_NAME);
    }


    public boolean hasWeaponTags() {
        if (this.itemStack.getTagCompound() == null) {
            return false;
        }
        return this.itemStack.getTagCompound().hasKey(COMPOUND_NAME);
    }

    public static boolean isMagazine(ItemStack itemStack) {
        if (itemStack.getTagCompound() == null) {
            return false;
        }
        String type = itemStack.getTagCompound().getCompoundTag("module").getString("type");
//        System.out.println("type: " + type);
        return type.contains("магазин");
    }

    public NBTTagCompound getWeaponTags() {
        if (tagCompound.hasKey(COMPOUND_NAME)) {
            return tagCompound.getCompoundTag(COMPOUND_NAME);
        }
        return null;
    }


    public void addWeaponTag(NBTTagCompound weapontag) {
        NBTTagCompound weapontags = getWeaponTagsOrCreate();
        weapontags.merge(weapontag);
        //weapontags.setTag(weapon, weapontag);
        tagCompound.setTag(COMPOUND_NAME, weapontags);
    }

    public void addWeaponTagByName(String weaponname, NBTTagCompound weapontag) {
        NBTTagCompound weapontags = getWeaponTagsOrCreate();
        //System.out.println("weapontags: " + weapontags.toString());
        weapontags.setTag(weaponname, weapontag);
        //weapontags.setTag(weapon, weapontag);
        //System.out.println("weapontags: " + weapontags.toString());
        tagCompound.setTag(COMPOUND_NAME, weapontags);
        //System.out.println("tagCompound: " + tagCompound);
    }

    public void setDefaultWeaponKey(String defaultWeapon) {
        //System.out.println("HEY HEY HEY");
        NBTTagCompound weapontags = getWeaponTagsOrCreate();
        //System.out.println("weapontags: " + weapontags.toString());
        weapontags.setString(DEFAULT_WEAPON, defaultWeapon);
        //weapontags.setTag(weapon, weapontag);
        //System.out.println("weapontags: " + weapontags.toString());
        tagCompound.setTag(COMPOUND_NAME, weapontags);
        //System.out.println("tagCompound: " + tagCompound);
    }


    public void delWeaponTag(String weapon) throws LoreException {
        if (this.tagCompound == null) {
            throw new LoreException("У предмета и без того нет тегов оружия.");
        }

        if (tagCompound.hasNoTags() || !tagCompound.hasKey(COMPOUND_NAME)) {
            throw new LoreException("У предмета и без того нет тегов оружия.");
        }

        if (!getWeaponTagsOrCreate().hasKey(weapon)) {
            throw new LoreException("У предмета и без того нет такого тега оружия.");
        }

        getWeaponTagsOrCreate().removeTag(weapon);
    }


    public int getWeaponRange() {
        return getDefaultWeapon().getCompoundTag(MELEE_KEY).getInteger(REACH_KEY);
    }

    public int getWeaponRange(String name) {
        return getWeaponByName(name).getCompoundTag(MELEE_KEY).getInteger(REACH_KEY);

    }

    public void changeDefaultWeaponToNext() {
        boolean goodToGo = false;
        String firstInSet = "";
        int i = 0;
        for (String weaponKey : this.getWeaponTags().getKeySet()) {
            System.out.println(i++ + ": " + weaponKey);
            if (weaponKey.equals(DEFAULT_WEAPON)) continue;
            if (firstInSet.isEmpty()) firstInSet = weaponKey;
            if (goodToGo) {
                setDefaultWeaponKey(weaponKey);
                System.out.println("Default weapon key set to " + weaponKey);
                return;
            }
            if (getDefaultTag().equals(weaponKey)) {
                goodToGo = true; // we found default tag. Next one in KeySet is our pick
            }
        }
        setDefaultWeaponKey(firstInSet);
        System.out.println("Default weapon key set to " + firstInSet);

        return;
    }

    public NBTTagCompound getAttachmentBySlot(int slot) {
        if (itemStack.hasTagCompound() && itemStack.getTagCompound() != null) {
            if (itemStack.getTagCompound().hasKey("attachments")) {
                if (itemStack.getTagCompound().getCompoundTag("attachments").hasKey(String.valueOf(slot))) {
                    return itemStack.getTagCompound().getCompoundTag("attachments").getCompoundTag(String.valueOf(slot));
                }
            }
        }
        return null;
    }

    public NBTTagCompound getAttachmentByWeaponName(String weaponName) {
        WeaponTagsHandler weaponTagsHandler = new WeaponTagsHandler(itemStack);
        if(weaponTagsHandler.getWeaponTags().hasKey(weaponName)) {
            NBTTagCompound weaponTag = weaponTagsHandler.getWeaponTags().getCompoundTag(weaponName);
            if(weaponTag.hasKey("attachment")) {
                int slot = weaponTag.getInteger("attachment");
                if (itemStack.hasTagCompound() && itemStack.getTagCompound() != null) {
                    if (itemStack.getTagCompound().hasKey("attachments")) {
                        if (itemStack.getTagCompound().getCompoundTag("attachments").hasKey(String.valueOf(slot))) {
                            return itemStack.getTagCompound().getCompoundTag("attachments").getCompoundTag(String.valueOf(slot));
                        }
                    }
                }
            }
        }
        return null;
    }


    public List<NBTTagCompound> getAllWeapons() {
        List<NBTTagCompound> result = new ArrayList<>();
        for (String weaponKey : this.getWeaponTags().getKeySet()) {
            result.add(this.getWeaponTags().getCompoundTag(weaponKey));
        }
        return result;
    }

    public NBTTagCompound getWeaponByName(String name) {
        return this.getWeaponTags().getCompoundTag(name);
    }

    public String getWeaponMod(String name) {
        return getWeaponByName(name).getString(DAMAGE_KEY);
    }

    public String getWeaponMod() {
        NBTTagCompound defaultWeapon = getDefaultWeapon();
        return defaultWeapon.getString(DAMAGE_KEY);
    }

    public String getWeaponPsychMod(String name) {
        return getWeaponByName(name).getString(PSYCHDAMAGE_KEY);
    }

    public String getWeaponPsychMod() {
        NBTTagCompound defaultWeapon = getDefaultWeapon();
        return defaultWeapon.getString(PSYCHDAMAGE_KEY);
    }

    public int getWeaponDefenceMod(String name) {
        return getWeaponByName(name).getInteger(DEFENCEMOD_KEY);
    }

    public int getWeaponDefenceMod() {
        NBTTagCompound defaultWeapon = getDefaultWeapon();
        return defaultWeapon.getInteger(DEFENCEMOD_KEY);
    }

    public int getWeaponAccuracyMod(String name) {
        return getWeaponByName(name).getInteger(ACCURACY_KEY);
    }

    public int getWeaponAccuracyMod() {
        NBTTagCompound defaultWeapon = getDefaultWeapon();
        return defaultWeapon.getInteger(ACCURACY_KEY);
    }

    public String getWeaponPerk(String name) {
        return getWeaponByName(name).getString(PERK_KEY);
    }

    public String getWeaponPerk() {
        NBTTagCompound defaultWeapon = getDefaultWeapon();
        return defaultWeapon.getString(PERK_KEY);
    }

    // DEFAULT WEAPON

    public boolean isWeaponDefault(String weaponName) {
        return weaponName.equals(getDefaultTag());
    }

    public String getDefaultWeaponName() {
        //System.out.println("1");
        if (!getDefaultTag().isEmpty()) return getDefaultTag();
        //System.out.println("2");
        //System.out.println(this.getWeaponTags().getKeySet());
        for (String weaponKey : this.getWeaponTags().getKeySet()) {
            return weaponKey;
        }
        //System.out.println("3");
        return null;
    }

    public NBTTagCompound getDefaultWeapon() {
        return getWeaponByName(getDefaultWeaponName());
        /*
        for (String weaponKey : this.getWeaponTags().getKeySet()) {
            if (weaponKey.equals(defaultTag)) return getWeaponByName(weaponKey);
            //return getWeaponByName(weaponKey);
        }
        // return first occurrence if no default set
        return getWeaponByName((String) this.getWeaponTags().getKeySet().toArray()[0]);

         */
    }

    public String getDefaultTag() {
        String def = "";
        if (getWeaponTags().hasKey(DEFAULT_WEAPON)) {
            def = getWeaponTags().getString(DEFAULT_WEAPON);
            if (getWeaponTags().hasKey(def)) return def;
        }
        for (String weaponKey : this.getWeaponTags().getKeySet()) {
            if(weaponKey.equals(DEFAULT_WEAPON)) continue;
            setDefaultWeaponKey(weaponKey);
            return weaponKey;
        }
        return def;
    }

    public void setDefaultTagIfNone() {
        if (getWeaponTags().hasKey(DEFAULT_WEAPON)) {
            String def = getWeaponTags().getString(DEFAULT_WEAPON);
            if (getWeaponTags().hasKey(def)) return;
        }
        String key = "";
        for (String weaponKey : this.getWeaponTags().getKeySet()) {
            if(weaponKey.equals(DEFAULT_WEAPON)) continue;
            System.out.println("putting " + key + " as default");
            key = weaponKey;
            break;
        }
        if (!key.isEmpty()) setDefaultWeaponKey(key);
    }

    public String getDefaultTagNameClient() {
        if (getWeaponTags().hasKey(DEFAULT_WEAPON)) {
            String def = getWeaponTags().getString(DEFAULT_WEAPON);
            if (getWeaponTags().hasKey(def)) return def;
        }
        return "";
    }



    // ПРОВЕРКИ

    public boolean isWeapon() {
        if (!hasWeaponTags()) return false;
        for (NBTTagCompound weapon : getAllWeapons()) {
            for (String key : WEAPON_KEYS) {
                if (weapon.hasKey(key))
                    return true;
            }
        }
        return false;
    }

    public boolean isStatusEffect() {
        if (!hasWeaponTags()) return false;
        for (NBTTagCompound weapon : getAllWeapons()) {
            for (String key : STATUS_KEYS) {
                if (weapon.hasKey(key))
                    return true;
            }
        }
        return false;
    }


    public boolean isArmor() {
        if (!hasWeaponTags()) return false;
        for (NBTTagCompound weapon : getAllWeapons()) {
            for (String key : ARMOR_KEYS) {
                if (weapon.hasKey(key))
                    return true;
            }
        }
        return false;
    }

    public boolean isShield() {
        if (!hasWeaponTags()) return false;
        for (NBTTagCompound weapon : getAllWeapons()) {
            for (String key : SHIELD_KEYS) {
                if (weapon.hasKey(key))
                    return true;
            }
        }
        return false;
    }

    public boolean isBigShield() {
        if (!hasWeaponTags()) return false;
        for (NBTTagCompound weapon : getAllWeapons()) {
            for (String key : SHIELD_KEYS) {
                if (weapon.hasKey(key)) {
                    String category = weapon.getCompoundTag(key).getString("category");
                    if(category.equals("большой") || category.equals("стационарный")) return true;
                }
            }
        }
        return false;
    }

    public boolean isFirearm() {
        if (!hasWeaponTags()) return false;
//        for (NBTTagCompound weapon : getAllWeapons()) {
//            if (weapon.hasKey(FIREARM_KEY))
//                return true;
//        }
        return getDefaultWeapon().hasKey(FIREARM_KEY);
    }

    public boolean isAir() {
        String name = this.itemStack.getItem().getUnlocalizedName();
        System.out.println("ITEM NAME IS " + name);
        if (name.equals("air")) {
            return true;
        }
        return false;
    }

    public boolean isUnarmed() {
        if (!hasWeaponTags()) return true;
        for (NBTTagCompound weapon : getAllWeapons()) {
            for (String key : UNARMED_KEYS) {
                if (weapon.hasKey(key))
                    return true;
            }
        }
        return false;
    }

    public boolean isSerial() {
        //System.out.println(getDefaultWeapon());
        if (!getDefaultWeapon().getCompoundTag(FIREARM_KEY).hasKey("serial")) return false;
        return getDefaultWeapon().getCompoundTag(FIREARM_KEY).getBoolean("serial");
    }

    public boolean isDouble() {
        //System.out.println(getDefaultWeapon());
        if (!getDefaultWeapon().getCompoundTag(FIREARM_KEY).hasKey("double")) return false;
        return getDefaultWeapon().getCompoundTag(FIREARM_KEY).getBoolean("double");
    }

    public boolean isMachineGun() {
        return getDefaultWeapon().getCompoundTag(FIREARM_KEY).getString("type").equals("пулемёт");
    }


    public String getCaliber() {
        if (!isFirearm()) return null;
        return getDefaultWeapon().getCompoundTag(FIREARM_KEY).getString("caliber");
    }

    public String getRangeType() {
        if (!isFirearm()) return null;
        return getDefaultWeapon().getCompoundTag(FIREARM_KEY).getString("range");
    }

    public boolean isFirearmLoaded() {
        if (!isFirearm()) return false;
        //System.out.println("Loaded shit: " + getDefaultWeapon().getCompoundTag(FIREARM_KEY).getTagList(CHAMBER, 10).tagCount());
        return getDefaultWeapon().getCompoundTag(FIREARM_KEY).getTagList(CHAMBER, 10).tagCount() > 0;
    }

    public boolean isFirearmLoadedByName(String name) {
        if (!isFirearm()) return false;
        //System.out.println("Loaded shit: " + getDefaultWeapon().getCompoundTag(FIREARM_KEY).getTagList(CHAMBER, 10).tagCount());
        return getWeaponByName(name).getCompoundTag(FIREARM_KEY).getTagList(CHAMBER, 10).tagCount() > 0;
    }

    public boolean isFull() {
        if (!isFirearm()) return false;
        return getDefaultWeapon().getCompoundTag(FIREARM_KEY).getTagList(CHAMBER, 10).tagCount() == getDefaultWeapon().getCompoundTag(FIREARM_KEY).getInteger("capacity");
    }


    public boolean doesProjectileFit(NBTTagCompound projectile) {
        return getCaliber().equals(projectile.getString("caliber"));
    }


    public void loadProjectileToChamber(NBTTagCompound projectile, String itemRL) {
        projectile.setString("itemrl", itemRL);
        loadProjectileToChamber(projectile);
    }

    public void loadProjectileToChamber(NBTTagCompound projectile) {

        if (itemStack.getTagCompound() != null) {
            NBTTagCompound weapontags = getWeaponTagsOrCreate();
            NBTTagCompound weapon = getDefaultWeapon();
            NBTTagList chamber = weapon.getCompoundTag(FIREARM_KEY).getTagList(CHAMBER, 10);
            chamber.appendTag(projectile);
            getDefaultWeapon().getCompoundTag(FIREARM_KEY).setTag(CHAMBER, chamber);

            weapontags.setTag(getDefaultWeaponName(), weapon);
            this.tagCompound.setTag(COMPOUND_NAME, weapontags);

        }

    }

    public NBTTagCompound getLastProjectile() {
        if (!isFirearmLoaded()) return null;
        NBTTagCompound weapontags = getWeaponTagsOrCreate();
        NBTTagCompound weapon = getDefaultWeapon();
        NBTTagList chamber = weapon.getCompoundTag(FIREARM_KEY).getTagList(CHAMBER, 10);
        return (NBTTagCompound) chamber.get(chamber.tagCount() - 1);
    }

    public void popLastProjectile() {
        if (!isFirearmLoaded()) return;
        NBTTagCompound weapontags = getWeaponTagsOrCreate();
        NBTTagCompound weapon = getDefaultWeapon();
        NBTTagList chamber = weapon.getCompoundTag(FIREARM_KEY).getTagList(CHAMBER, 10);
        chamber.removeTag(0);
    }

    public void fireProjectile() {
        if (!isFirearmLoaded()) return;
        NBTTagCompound weapontags = getWeaponTagsOrCreate();
        NBTTagCompound weapon = getDefaultWeapon();
        NBTTagList chamber = weapon.getCompoundTag(FIREARM_KEY).getTagList(CHAMBER, 10);

        System.out.println("Firing " + chamber.get(chamber.tagCount() - 1));
        chamber.removeTag(chamber.tagCount() - 1);
        getDefaultWeapon().getCompoundTag(FIREARM_KEY).setTag(CHAMBER, chamber);

        weapontags.setTag(getDefaultWeaponName(), weapon);
        this.tagCompound.setTag(COMPOUND_NAME, weapontags);
    }

    public NBTTagCompound getOptic() {
        NBTTagCompound optic = this.itemStack.getTagCompound().getCompoundTag(AttachmentsHandler.ATTACHMENTS).getCompoundTag("0").getCompoundTag("module");
        System.out.println("optic: " + optic);
        return optic;
    }

    public NBTTagCompound getAccessory() {
        NBTTagCompound accessory = this.itemStack.getTagCompound().getCompoundTag(AttachmentsHandler.ATTACHMENTS).getCompoundTag("1").getCompoundTag("module");
        System.out.println("accessory: " + accessory);
        return accessory;
    }

    public NBTTagCompound getBarrel() {
        NBTTagCompound barrel = this.itemStack.getTagCompound().getCompoundTag(AttachmentsHandler.ATTACHMENTS).getCompoundTag("2").getCompoundTag("module");
        System.out.println("barrel: " + barrel);
        return barrel;
    }

    public NBTTagCompound getUnderbarrel() {
        NBTTagCompound underbarrel = this.itemStack.getTagCompound().getCompoundTag(AttachmentsHandler.ATTACHMENTS).getCompoundTag("3").getCompoundTag("module");
        System.out.println("underbarrel: " + underbarrel);
        return underbarrel;
    }


    public NBTTagCompound getExtra() {
        NBTTagCompound extra = this.itemStack.getTagCompound().getCompoundTag(AttachmentsHandler.ATTACHMENTS).getCompoundTag("4").getCompoundTag("module");
        System.out.println("extra: " + extra);
        return extra;
    }

    public NBTTagCompound getAmmunition() {
        NBTTagCompound ammunition = this.itemStack.getTagCompound().getCompoundTag(AttachmentsHandler.ATTACHMENTS).getCompoundTag("5").getCompoundTag("module");
        System.out.println("ammunition: " + ammunition);
        return ammunition;
    }

    public int getModuleMods() {
        return getOptic().getInteger("mod") +
                getBarrel().getInteger("mod") +
                getUnderbarrel().getInteger("mod") +
                getAccessory().getInteger("mod") +
                getExtra().getInteger("mod") +
                getAmmunition().getInteger("mod") +
                getMagazine().getCompoundTag("module").getInteger("mod");
    }


    public NBTTagCompound getMagazine() {
        NBTTagCompound magazine = this.itemStack.getTagCompound().getCompoundTag(AttachmentsHandler.ATTACHMENTS).getCompoundTag("6");
        //System.out.println("magazine: " + magazine);
        return magazine;
    }
}
